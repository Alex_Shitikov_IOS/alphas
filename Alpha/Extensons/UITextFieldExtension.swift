//
//  UITextFieldExtension.swift
//  Pcounter
//
//  Created by alexandr shitikov on 13.02.2020.
//  Copyright © 2020 Intranso. All rights reserved.
//

import UIKit

struct PCTextFieldMaxLenght {
    static let password:Int = 256
    static let email:Int = 256
    static let defaultLimit: Int = 256
}

//regex for validation
fileprivate let emailRegex    = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
fileprivate let passwordRegex = "^.{6,}$"
fileprivate let phoneRegex    = "^[0-9]{10}$"
fileprivate let textRegex     = "^.{3,}$"
fileprivate let textTwoRegex  = "^.{2,}$"
fileprivate let notEmptyRegex  = "^.{1,}$"

private var __maxLengths = [UITextField: Int]()
extension UITextField {
    
    func setLeftView(image: UIImage) {
        let iconView = UIImageView(frame: CGRect(x: 10, y: 8, width: 20, height: 20)) // set your Own size
        iconView.image = image
        let iconContainerView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 35, height: 35))
        iconContainerView.addSubview(iconView)
        leftView = iconContainerView
        leftViewMode = .always
        self.tintColor = .lightGray
    }
    
    //MARK: Max length
    
    /**you can set preset values from  PCTextFieldMaxLenght */
    @IBInspectable var maxLength: Int {
        get {
            guard let l = __maxLengths[self] else {
                return PCTextFieldMaxLenght.defaultLimit // (global default-limit)
            }
            return l
        }
        set {
            __maxLengths[self] = newValue
            addTarget(self, action: #selector(fix), for: .editingChanged)
        }
    }
    
    //MARK: Placeholder color
    
    @IBInspectable var placeholderColor: UIColor {
        get {
            return attributedPlaceholder?.attribute(.foregroundColor, at: 0, effectiveRange: nil) as? UIColor ?? .clear
        }
        set {
            guard let attributedPlaceholder = attributedPlaceholder else { return }
            let attributes: [NSAttributedString.Key: UIColor] = [.foregroundColor: newValue]
            self.attributedPlaceholder = NSAttributedString(string: attributedPlaceholder.string, attributes: attributes)
        }
    }
    
    @objc func fix(textField: UITextField) {
        guard let t = textField.text else {return}
        textField.text = String(t.prefix(maxLength))
    }
    
    //MARK: Validation
    
    func isValidEmail() -> Bool {
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegex)
        return emailTest.evaluate(with: text)
    }
    
    func isValidPassword() -> Bool {
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", passwordRegex)
        return passwordTest.evaluate(with: text)
    }
    
    func isValidEmpty() -> Bool {
        return !self.text!.isEmpty 
    }
}
