//
//  ALColors.swift
//  Alpha
//
//  Created by alexandr shitikov on 08.09.2020.
//  Copyright © 2020 alexandr shitikov. All rights reserved.
//

import UIKit


extension UIColor {
    
    public convenience init?(hex: String) {
        let r, g, b, a: CGFloat
        
        if hex.hasPrefix("#") {
            let start = hex.index(hex.startIndex, offsetBy: 1)
            let hexColor = String(hex[start...])
            
            if hexColor.count == 8 {
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0
                
                if scanner.scanHexInt64(&hexNumber) {
                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    a = CGFloat(hexNumber & 0x000000ff) / 255
                    
                    self.init(red: r, green: g, blue: b, alpha: a)
                    return
                }
            }
        }
        return nil
    }
    
    @nonobjc class var inputBackground: UIColor {
        return UIColor(red: 0, green: 0, blue: 0, alpha: 0.05)
    }
    
    @nonobjc class var inputError: UIColor {
        return UIColor(red: 1.0, green: 0.24, blue: 0.24, alpha: 1.0)
    }
    
    @nonobjc class var inputPlaceholder: UIColor {
        return UIColor(red: 0, green: 0, blue: 0, alpha: 0.03)
    }
    
    @nonobjc class var orangeButton: UIColor {
        return UIColor(red: 0.965, green: 0.569, blue: 0.067, alpha: 1)
    }
    
    @nonobjc class var lightOrangeButton: UIColor {
        return UIColor(red: 1, green: 0.773, blue: 0.337, alpha: 1)
    }
    
    @nonobjc class var textBlack: UIColor {
        return UIColor(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
    @nonobjc class var textGray: UIColor {
        return UIColor(red: 0.557, green: 0.557, blue: 0.576, alpha: 1)
    }
    
    @nonobjc class var mainTextBlack: UIColor {
        return .black
    }
    
    //chat
    @nonobjc class var outsideMessage: UIColor {
        return UIColor(red: 0.949, green: 0.949, blue: 0.949, alpha: 1)
    }
    
    @nonobjc class var currentUserMessage: UIColor {
        return UIColor(red: 0.992, green: 0.914, blue: 0.812, alpha: 1)
        
    }
    
    @nonobjc class var black05: UIColor {
        return UIColor.black.withAlphaComponent(0.5)
    }
    
    //E6E6E6
    
    @nonobjc class var searchBarBackground: UIColor {
        return UIColor(red: 0.902, green: 0.902, blue: 0.902, alpha: 1.00)
    }
    
    @nonobjc class var navigationGray: UIColor {
        return UIColor(red: 0.949, green: 0.949, blue: 0.949, alpha: 1.00)
    }
    
}
