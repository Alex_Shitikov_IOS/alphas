//
//  ALAlert.swift
//  Alpha
//
//  Created by alexandr shitikov on 11.07.2020.
//  Copyright © 2020 alexandr shitikov. All rights reserved.
//

import UIKit

extension UIAlertController {

    static func alert1(title:String, message:String = "", action:UIAlertAction?, target:UIViewController) {
        target.view.endEditing(true)
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        if let customAction = action {
            alert.addAction(customAction)
        } else {
            let okAction = UIAlertAction(title: "Oк", style: .cancel, handler: nil)
            alert.addAction(okAction)
        }
        target.present(alert, animated: true, completion: nil)
    }
}
