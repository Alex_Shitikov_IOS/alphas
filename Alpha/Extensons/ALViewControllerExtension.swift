//
//  ALViewControllerEctension.swift
//  Alpha
//
//  Created by alexandr shitikov on 29.09.2020.
//  Copyright © 2020 alexandr shitikov. All rights reserved.
//

import UIKit

extension UIViewController {
    static func loadFromNib() -> Self {
        func instantiateFromNib<T: UIViewController>() -> T {
            return T.init(nibName: String(describing: T.self), bundle: nil)
        }
        return instantiateFromNib()
    }
}
