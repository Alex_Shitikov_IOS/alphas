//
//  ALResponse.swift
//  Alpha
//
//  Created by alexandr shitikov on 12.07.2020.
//  Copyright © 2020 alexandr shitikov. All rights reserved.
//

import Foundation

extension URLResponse {
    //Check response status code in range from 200 - 299
    func isSuccess() -> Bool {
        guard let response = self as? HTTPURLResponse else { return false }
        return CLSuccessCode.isSuccessCode(code: response.statusCode)
    }
}

//************************

enum CLErrorCode: Int, Codable {
    case badCredentials = 400
    case serverError = 500
    case unauthorized = 401
}

enum CLSuccessCode: Int {
    case success = 200
    
    static func isSuccessCode(code:Int) -> Bool {
        for success in 200..<299 {
            if code == success {
                return true
            }
        }
        return false
    }
}


    
    
