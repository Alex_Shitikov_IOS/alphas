//
//  ALData.swift
//  Alpha
//
//  Created by alexandr shitikov on 19.07.2020.
//  Copyright © 2020 alexandr shitikov. All rights reserved.
//

import Foundation

extension Data {
    init(reading input: InputStream) {
        self.init()
        input.open()
        
        let bufferSize = 1024*100
        let buffer = UnsafeMutablePointer<UInt8>.allocate(capacity: bufferSize)
        while input.hasBytesAvailable {
            let read = input.read(buffer, maxLength: bufferSize)
            if read > 0 {
                self.append(buffer, count: read)
            }
            
        }
        buffer.deallocate()
    }
    
    func parseCodableObject <T:Codable>( structType:T.Type) -> T? {
        do {
            let item = try JSONDecoder().decode(structType.self, from: self)
            return item
        } catch {
            print("\(structType)->\(error)")
            return nil
        }
        
    }
    func getStringSizeInMB(size: Int) -> String {
        let byteCount = size
        let bcf = ByteCountFormatter()
        bcf.allowedUnits = [.useMB]
        bcf.countStyle = .file
        let string = bcf.string(fromByteCount: Int64(byteCount))
        let trimmedString = string.components(separatedBy: " ").first ?? "0.0"
        return trimmedString
    }
    
    func getDoubleSizeInMB() -> Double {
        let byteCount = Double(self.count)
        let mbSize = byteCount / (1024 * 1024)
        return mbSize
    }
}
