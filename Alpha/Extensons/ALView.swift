//
//  ALView.swift
//  Alpha
//
//  Created by alexandr shitikov on 15.07.2020.
//  Copyright © 2020 alexandr shitikov. All rights reserved.
//

import UIKit
import Lottie

enum Direction {
    case horizontal
    case vertical
}


extension UIView {
    
    var heightConstraint: NSLayoutConstraint? {
        get {
            return constraints.first(where: {
                $0.firstAttribute == .height && $0.relation == .equal
            })
        }
        set { setNeedsLayout() }
    }
    
    var widthConstraint: NSLayoutConstraint? {
        get {
            return constraints.first(where: {
                $0.firstAttribute == .width && $0.relation == .equal
            })
        }
        set { setNeedsLayout() }
    }
    
    
    func rounded(radius: Double? = nil) {
        if radius != nil {
            self.layer.cornerRadius = CGFloat(radius!)
        } else {
            self.layer.cornerRadius = self.frame.size.height / 2
        }
    }
    
    func setDefaultShadow() {
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowRadius = 2.0
        layer.shadowOffset = .zero
        layer.shadowOpacity = 0.2
    }
    
    func setLottieAnimation(animationName:String, loop:Bool) {
        let animationView = AnimationView(name:animationName)
        animationView.frame = self.frame
        animationView.center = CGPoint(x: self.frame.width/2, y: self.frame.height/2)
        self.addSubview(animationView)
        animationView.loopMode = loop ? LottieLoopMode.loop: LottieLoopMode.playOnce
        animationView.play()
    }
    
    func setBorderGradient(colors: [UIColor], lineWidth: CGFloat = 1, direction: Direction = .horizontal, cornerRadius: CGFloat, animation:Bool = false) {
        let sublayers = layer.sublayers
        sublayers?.forEach({ (gr) in
            if gr.name == "BorderGradient" {
                gr.removeFromSuperlayer()
            }
        })
        self.layer.cornerRadius = cornerRadius
        self.layer.masksToBounds = true
        let gradient = CAGradientLayer()
        gradient.name = "BorderGradient"
        gradient.frame = CGRect(origin: CGPoint.zero, size: self.frame.size)
        gradient.colors = colors.map({ (color) -> CGColor in
            color.cgColor
        })
        
        if animation {
            let gradientChangeAnimation = CABasicAnimation(keyPath: "colors")
            gradientChangeAnimation.duration = 2.0
            gradientChangeAnimation.repeatCount = Float.infinity
            gradientChangeAnimation.fromValue = [colors.first?.cgColor]
            gradientChangeAnimation.toValue = [colors.last?.cgColor]
            gradientChangeAnimation.fillMode = CAMediaTimingFillMode.forwards
            gradientChangeAnimation.isRemovedOnCompletion = false
            gradient.add(gradientChangeAnimation, forKey: "locations")
        }
        
        switch direction {
        case .horizontal:
            gradient.startPoint = CGPoint(x: 0, y: 1)
            gradient.endPoint = CGPoint(x: 1, y: 1)
        case .vertical:
            gradient.startPoint = CGPoint(x: 0, y: 0)
            gradient.endPoint = CGPoint(x: 0, y: 1)
        }
        
        let shape = CAShapeLayer()
        shape.lineWidth = lineWidth
        shape.path = UIBezierPath(roundedRect: self.bounds.insetBy(dx: lineWidth,
                                                                   dy: lineWidth), cornerRadius: cornerRadius).cgPath
        shape.strokeColor = UIColor.black.cgColor
        shape.fillColor = UIColor.clear.cgColor
        gradient.mask = shape
        
        self.layer.addSublayer(gradient)
    }
    
    func setGradientBackground(colors: [UIColor], lineWidth: CGFloat = 1, direction: Direction = .horizontal, cornerRadius: CGFloat, animation:Bool = false) {
        let sublayers = layer.sublayers
        sublayers?.forEach({ (gr) in
            if gr.name == "Gradient" {
                gr.removeFromSuperlayer()
            }
        })
        self.layer.cornerRadius = cornerRadius
        self.layer.masksToBounds = true
        let gradient = CAGradientLayer()
        gradient.name = "Gradient"
        gradient.frame = CGRect(origin: CGPoint.zero, size: self.frame.size)
        gradient.colors = colors.map({ (color) -> CGColor in
            color.cgColor
        })
        
        if animation {
            let gradientChangeAnimation = CABasicAnimation(keyPath: "colors")
            gradientChangeAnimation.duration = 2.0
            gradientChangeAnimation.repeatCount = Float.infinity
            gradientChangeAnimation.fromValue = [colors.first?.cgColor]
            gradientChangeAnimation.toValue = [colors.last?.cgColor]
            gradientChangeAnimation.fillMode = .forwards
            gradientChangeAnimation.isRemovedOnCompletion = false
            gradient.add(gradientChangeAnimation, forKey: "locations")
        }
        
        switch direction {
        case .horizontal:
            gradient.startPoint = CGPoint(x: 0, y: 1)
            gradient.endPoint = CGPoint(x: 1, y: 1)
        case .vertical:
            gradient.startPoint = CGPoint(x: 0, y: 0)
            gradient.endPoint = CGPoint(x: 0, y: 1)
        }
        //        self.layer.addSublayer(gradient)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.layer.insertSublayer(gradient, at: 0)
        }
        
    }
    
    func add(border: ViewBorder, color: UIColor, height: CGFloat, leftOffset:CGFloat) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            let borderLayer = CALayer()
            borderLayer.backgroundColor = color.cgColor
            borderLayer.name = border.rawValue
            switch border {
            case .top:
                borderLayer.frame = CGRect(x: leftOffset, y: 0 + height, width: self.frame.size.width - leftOffset, height: height)
            case .bottom:
                borderLayer.frame = CGRect(x: leftOffset, y: self.frame.size.height - height, width: self.frame.size.width - leftOffset, height: height)
            case .right:
                borderLayer.frame = CGRect(x: self.frame.maxX - height, y: 0, width: height, height: self.frame.height)
            case .center:
                borderLayer.frame = CGRect(x: self.frame.midX, y: 0, width: height, height: self.frame.height)
            }
            self.layer.addSublayer(borderLayer)
        }
    }
    
    func remove(border: ViewBorder) {
        guard let sublayers = self.layer.sublayers else { return }
        var layerForRemove: CALayer?
        for layer in sublayers {
            if layer.name == border.rawValue {
                layerForRemove = layer
            }
        }
        if let layer = layerForRemove {
            layer.removeFromSuperlayer()
        }
    }
    
    func startLoader() {
        let loaderView = UIView(frame: CGRect(x: 0, y: 0, width: frame.width, height:frame.height))
        loaderView.tag = 789
        loaderView.backgroundColor = .clear
        let blurEffect = UIBlurEffect(style: .light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = loaderView.frame
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        blurEffectView.alpha = 0.1
        loaderView.addSubview(blurEffectView)
        // add loader image
        let animationImage = UIImageView(frame: CGRect(x: 0, y: 0, width: frame.width/3, height: frame.width/3))
        animationImage.image = #imageLiteral(resourceName: "Vector")
        animationImage.layer.shadowColor = UIColor.orangeButton.withAlphaComponent(0.2).cgColor
        animationImage.layer.shadowRadius = 2.0
        animationImage.layer.shadowOpacity = 0.5
        
        let animation = CABasicAnimation(keyPath: "transform.rotation.y")
        animation.fromValue = 0
        animation.toValue = 7 * Double.pi
        animation.duration = 2 // seconds
        animation.autoreverses = true
        animation.repeatCount = .infinity
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        animationImage.layer.add(animation, forKey: "rotation")
        var transform = CATransform3DIdentity
        transform.m34 = 1.0/200.0
        animationImage.layer.transform = transform
        
        
        blurEffectView.contentView.addSubview(animationImage)
        animationImage.startAnimating()
        animationImage.center = CGPoint(x: self.frame.width/2, y: self.frame.height/2.1)
        self.addSubview(loaderView)
        loaderView.bringSubviewToFront(blurEffectView.contentView)
        UIView.animate(withDuration: 0.5, animations: {
            blurEffectView.alpha = 0.9
        })
    }
    
    func stopLoader() {
        DispatchQueue.main.async {
            self.subviews.forEach { (view) in
                if view.tag == 789 {
                    UIView.animate(withDuration: 0.5, animations: {
                        view.alpha = 0
                    }) { (end) in
                        view.removeFromSuperview()
                    }
                }
            }
        }
    }
    
    
    func showPaceholder(type:ALAccountAccessType = .noAccess) {
        let holderView = ALAccountAccess(frame: CGRect(x: 0, y: 0, width: frame.width, height:frame.height))
        holderView.set(type: type)
        holderView.tag = 777
        let loaderView = UIView(frame: CGRect(x: 0, y: 0, width: frame.width, height:frame.height))
        loaderView.tag = 777
        loaderView.backgroundColor = .white
        holderView.backgroundColor = .white
        holderView.center = CGPoint(x: self.frame.width/2, y: self.frame.height/2)
        self.addSubview(holderView)
    }
    
    func hidePaceholder() {
        self.subviews.forEach { (view) in
            if view.tag == 777 || view is ALAccountAccess {
                UIView.animate(withDuration: 0.1, animations: {
                    view.alpha = 0
                }) { (end) in
                    view.removeFromSuperview()
                }
            }
        }
    }
    
    
}

enum ViewBorder: String {
    case  top, bottom, right, center
}
