//
//  ALDate.swift
//  Alpha
//
//  Created by alexandr shitikov on 18.10.2020.
//  Copyright © 2020 alexandr shitikov. All rights reserved.
//

import Foundation

enum DateFormat:String {
    case dd_MM_yyy =  "dd.MM.yyyy"
    case HH_mm = "HH:mm"
    case yyyy_MM_dd = "yyyy-MM-dd"
    case EEEE = "EEEE"
    case yyyy_MM_dd_HH_mm_ss = "yyyy-MM-dd HH:mm:ss"
    case dd_month_yyyy_HH_mm = "dd MMMM yyyy HH:mm"
    case dd_month_yyyy = "dd MMMM yyyy"
    case dd_mm_yyyy_HH_mm = "dd.MM.yyyy HH:mm"
    case passangerDate = "dd/MM/yyyy HH:mm:ss"
    //25/03/2020 07:08:57
}

extension Date {
    
    func string(withFormat:DateFormat, zeroZone:Bool = false) -> String {
        let dateFormatter = DateFormatter()
        if zeroZone == true {
            dateFormatter.timeZone = NSTimeZone(abbreviation: "GMT+0:00") as TimeZone?
        }
        dateFormatter.dateFormat = withFormat.rawValue
        return dateFormatter.string(from: self)
    }
}
