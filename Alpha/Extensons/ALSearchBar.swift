//
//  ALSearchBar.swift
//  Alpha
//
//  Created by alexandr shitikov on 09.10.2020.
//  Copyright © 2020 alexandr shitikov. All rights reserved.
//

import UIKit

extension UISearchBar {
    public var textField: UITextField? {
        if #available(iOS 13.0, *) {
            return searchTextField
        }

        guard let firstSubview = subviews.first else {
            assertionFailure("Could not find text field")
            return nil
        }

        for view in firstSubview.subviews {
            if let textView = view as? UITextField {
                return textView
            }
        }

        assertionFailure("Could not find text field")

        return nil
    }
}
