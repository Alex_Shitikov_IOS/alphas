//
//  ALEncodable.swift
//  Alpha
//
//  Created by alexandr shitikov on 12.07.2020.
//  Copyright © 2020 alexandr shitikov. All rights reserved.
//

import Foundation

class DictionaryDecoder {
    private let decoder = JSONDecoder()
    var dateDecodingStrategy: JSONDecoder.DateDecodingStrategy {
        get { return decoder.dateDecodingStrategy }
        set { decoder.dateDecodingStrategy = newValue }
    }
    
    var keyDecodingStrategy: JSONDecoder.KeyDecodingStrategy {
        get { return decoder.keyDecodingStrategy }
        set { decoder.keyDecodingStrategy = newValue }
    }
    
    var dataDecodingStrategy: JSONDecoder.DataDecodingStrategy {
        get { return decoder.dataDecodingStrategy }
        set { decoder.dataDecodingStrategy = newValue }
    }
    
    var nonConformingFloatDecodingStrategy: JSONDecoder.NonConformingFloatDecodingStrategy {
        get { return decoder.nonConformingFloatDecodingStrategy }
        set { decoder.nonConformingFloatDecodingStrategy = newValue }
    }
    
    func decode<T>(_ type: T.Type, from dictionary: [String: Any]) throws -> T where T : Decodable {
        let data = try JSONSerialization.data(withJSONObject: dictionary, options: [])
        return try decoder.decode(type, from: data)
    }
}

extension Encodable {
    var dictionary: [String: Any]? {
        guard let data = try? JSONEncoder().encode(self) else { return nil }
        return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] }
    }
    
    func queryItems() -> String {
        let dic = dictionary!
        var components = URLComponents()
        print(components.url!)
        components.queryItems = dic.map {
            URLQueryItem(name: $0, value: String(describing: $1))
        }
        let customAllowedSet =  CharacterSet.init(charactersIn: "_+-").inverted
        return components.url?.absoluteString.addingPercentEncoding(withAllowedCharacters: customAllowedSet) ?? ""
    }
}

extension Dictionary {
    
    var json: String {
        let invalidJson = "Not a valid JSON"
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
            return String(bytes: jsonData, encoding: String.Encoding.utf8) ?? invalidJson
        } catch {
            return invalidJson
        }
    }
    
    func printJson() {
        print(json)
    }
    
}


