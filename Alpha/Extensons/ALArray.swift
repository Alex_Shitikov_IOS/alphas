//
//  ALArray.swift
//  Alpha
//
//  Created by alexandr shitikov on 07.10.2020.
//  Copyright © 2020 alexandr shitikov. All rights reserved.
//

import Foundation

extension Array {
    func all(where predicate: (Element) -> Bool) -> [Element]  {
        return self.compactMap { predicate($0) ? $0 : nil }
    }
}
