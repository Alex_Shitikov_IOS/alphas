//
//  ALConstants.swift
//  Alpha
//
//  Created by alexandr shitikov on 12.07.2020.
//  Copyright © 2020 alexandr shitikov. All rights reserved.
//

import Foundation
import UIKit

enum UserRegistrationState: String {
    case general = "GENERAL"
    case password = "PASSWORD"
    case details = "DETAILS"
    case complete = "COMPLETED"

}

enum UserStatus: String {
    case verificationRequest = "VERIFICATION_REQUEST"
    case verified = "VERIFIED"
    case unverified = "UNVERIFIED"
    case registration = "REGISTRATION"
    case blocked = "BLOCKED"
}

enum ALStoryboard: String {
    case Authorization
    case TabBar
    case Profile
    case MainEvents
    case Alert
    case Chat
    case Access
    case PhotoGallery
    
    var instance : UIStoryboard {
        return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
    }
    
    func initController(withId:String) -> UIViewController {
        return instance.instantiateViewController(withIdentifier: withId)
    }
}


struct ALiDevice {
    // iDevice detection code
    static let IS_IPAD             = UIDevice.current.userInterfaceIdiom == .pad
    static let IS_IPHONE           = UIDevice.current.userInterfaceIdiom == .phone
    static let IS_RETINA           = UIScreen.main.scale >= 2.0
    
    static let SCREEN_WIDTH        = Int(UIScreen.main.bounds.size.width)
    static let SCREEN_HEIGHT       = Int(UIScreen.main.bounds.size.height)
    static let SCREEN_MAX_LENGTH   = Int( max(SCREEN_WIDTH, SCREEN_HEIGHT) )
    static let SCREEN_MIN_LENGTH   = Int( min(SCREEN_WIDTH, SCREEN_HEIGHT) )
    
    static let IS_IPHONE_4_OR_LESS = IS_IPHONE && SCREEN_MAX_LENGTH  < 568
    static let IS_IPHONE_5         = IS_IPHONE && SCREEN_MAX_LENGTH == 568
    static let IS_IPHONE_6         = IS_IPHONE && SCREEN_MAX_LENGTH == 667
    static let IS_IPHONE_6P        = IS_IPHONE && SCREEN_MAX_LENGTH == 736
    static let IS_IPHONE_X         = IS_IPHONE && SCREEN_MAX_LENGTH  > 800
}

