
//  Created by alexandr shitikov.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.

import Foundation

extension String { 
    
    static var lc_serverErrorMessage : String {
        get {
            return "serverErrorMessage".localized()
        }} 

    static var lc_registrationSuccess : String {
        get {
            return "registrationSuccess".localized()
        }} 

    static var lc_emptyFieldError : String {
        get {
            return "emptyFieldError".localized()
        }} 

    static var lc_incorrectEmail : String {
        get {
            return "incorrectEmail".localized()
        }} 

    static var lc_emailInUse : String {
        get {
            return "emailInUse".localized()
        }} 

    static var lc_incorrectPhone : String {
        get {
            return "incorrectPhone".localized()
        }} 

    static var lc_lessThatSixChar : String {
        get {
            return "lessThatSixChar".localized()
        }} 

    static var lc_passwordMismatch : String {
        get {
            return "passwordMismatch".localized()
        }} 

    static var lc_chooseInterests : String {
        get {
            return "chooseInterests".localized()
        }} 

    static var lc_sixCharError : String {
        get {
            return "sixCharError".localized()
        }} 

    static var lc_loginScenePlaceholderLogin : String {
        get {
            return "loginScenePlaceholderLogin".localized()
        }} 

    static var lc_loginScenePlaceholderPassword : String {
        get {
            return "loginScenePlaceholderPassword".localized()
        }} 

    static var lc_loginScenePlaceholderForgotPassword : String {
        get {
            return "loginScenePlaceholderForgotPassword".localized()
        }} 

    static var lc_loginScenePlaceholderLoginButton : String {
        get {
            return "loginScenePlaceholderLoginButton".localized()
        }} 

    static var lc_loginScenePlaceholderRegistrationText : String {
        get {
            return "loginScenePlaceholderRegistrationText".localized()
        }} 

    static var lc_loginScenePlaceholderRegistrationButton : String {
        get {
            return "loginScenePlaceholderRegistrationButton".localized()
        }} 

    static var lc_loginScenePlaceholderWelcom : String {
        get {
            return "loginScenePlaceholderWelcom".localized()
        }} 

    static var lc_registrationRegisstrationButtonTitle : String {
        get {
            return "registrationRegisstrationButtonTitle".localized()
        }} 

    static var lc_registrationHaveAccount : String {
        get {
            return "registrationHaveAccount".localized()
        }} 

    static var lc_registrationLogin : String {
        get {
            return "registrationLogin".localized()
        }} 

    static var lc_registrationStepPlaceholder : String {
        get {
            return "registrationStepPlaceholder".localized()
        }} 

    static var lc_registrationChangePhotoButtonText : String {
        get {
            return "registrationChangePhotoButtonText".localized()
        }} 

    static var lc_inputName : String {
        get {
            return "inputName".localized()
        }} 

    static var lc_inputLastName : String {
        get {
            return "inputLastName".localized()
        }} 

    static var lc_inputPhone : String {
        get {
            return "inputPhone".localized()
        }} 

    static var lc_inputEmail : String {
        get {
            return "inputEmail".localized()
        }} 

    static var lc_inputPasswordCreate : String {
        get {
            return "inputPasswordCreate".localized()
        }} 

    static var lc_inputPassword : String {
        get {
            return "inputPassword".localized()
        }} 

    static var lc_inputConfirmPassword : String {
        get {
            return "inputConfirmPassword".localized()
        }} 

    static var lc_inputAbout : String {
        get {
            return "inputAbout".localized()
        }} 

    static var lc_inputMyInterests : String {
        get {
            return "inputMyInterests".localized()
        }} 

    static var lc_forgotConfirm : String {
        get {
            return "forgotConfirm".localized()
        }} 

    static var lc_forgotMainTitle : String {
        get {
            return "forgotMainTitle".localized()
        }} 

    static var lc_forgotVerifyPhoneTitle : String {
        get {
            return "forgotVerifyPhoneTitle".localized()
        }} 

    static var lc_forgotMessage : String {
        get {
            return "forgotMessage".localized()
        }} 

    static var lc_forgotMessagePartTwo : String {
        get {
            return "forgotMessagePartTwo".localized()
        }} 

    static var lc_forgotNoCodeDescription : String {
        get {
            return "forgotNoCodeDescription".localized()
        }} 

    static var lc_forgotSendCodeButton : String {
        get {
            return "forgotSendCodeButton".localized()
        }} 

    static var lc_forgotEnterCode : String {
        get {
            return "forgotEnterCode".localized()
        }} 

    static var lc_forgotConfirmCode : String {
        get {
            return "forgotConfirmCode".localized()
        }} 

    static var lc_forgotResendsuccessMessage : String {
        get {
            return "forgotResendsuccessMessage".localized()
        }} 

    static var lc_forgotNewPasswordTitle : String {
        get {
            return "forgotNewPasswordTitle".localized()
        }} 

    static var lc_forgotPasswordChangedsuccessMessage : String {
        get {
            return "forgotPasswordChangedsuccessMessage".localized()
        }} 

    static var lc_interestsTitle : String {
        get {
            return "interestsTitle".localized()
        }} 

    static var lc_InterestsSelectButton : String {
        get {
            return "InterestsSelectButton".localized()
        }} 

    static var lc_profileEvensLisTitle : String {
        get {
            return "profileEvensLisTitle".localized()
        }} 

    static var lc_profileShowAllButtonTitle : String {
        get {
            return "profileShowAllButtonTitle".localized()
        }} 

    static var lc_profileInformationTitle : String {
        get {
            return "profileInformationTitle".localized()
        }} 

    static var lc_profileContactsTitle : String {
        get {
            return "profileContactsTitle".localized()
        }} 

    static var lc_profileChangePasswordTitle : String {
        get {
            return "profileChangePasswordTitle".localized()
        }} 

    static var lc_profileExitTitle : String {
        get {
            return "profileExitTitle".localized()
        }} 

    static var lc_profileMyProfileTitle : String {
        get {
            return "profileMyProfileTitle".localized()
        }} 

    static var lc_profileSucceSaveChangesMessage : String {
        get {
            return "profileSucceSaveChangesMessage".localized()
        }} 

    static var lc_profileContactsTitleEdit : String {
        get {
            return "profileContactsTitleEdit".localized()
        }} 

    static var lc_PhotoPicker_galary : String {
        get {
            return "PhotoPicker_galary".localized()
        }} 

    static var lc_PhotoPicker_camera : String {
        get {
            return "PhotoPicker_camera".localized()
        }} 

    static var lc_PhotoPicker_title : String {
        get {
            return "PhotoPicker_title".localized()
        }} 

    static var lc_PhotoPicker_message : String {
        get {
            return "PhotoPicker_message".localized()
        }} 

    static var lc_PhotoPicker_cancel : String {
        get {
            return "PhotoPicker_cancel".localized()
        }} 

    static var lc_goodButtonTitle : String {
        get {
            return "goodButtonTitle".localized()
        }} 

    static var lc_checkTitle : String {
        get {
            return "checkTitle".localized()
        }} 

    static var lc_checkDescription : String {
        get {
            return "checkDescription".localized()
        }} 

    static var lc_photoGalleryTitle : String {
        get {
            return "photoGalleryTitle".localized()
        }} 

    static var lc_chatMainTitle : String {
        get {
            return "chatMainTitle".localized()
        }} 

    static var lc_favoriteContactsTitle : String {
        get {
            return "favoriteContactsTitle".localized()
        }} 

    static var lc_chatNewMessageTitle : String {
        get {
            return "chatNewMessageTitle".localized()
        }} 

    static var lc_chatCreateGroupTitle : String {
        get {
            return "chatCreateGroupTitle".localized()
        }} 

    static var lc_chatOrStardDialogTitle : String {
        get {
            return "chatOrStardDialogTitle".localized()
        }} 

    static var lc_chatOwnerDeletedChat : String {
        get {
            return "chatOwnerDeletedChat".localized()
        }} 

    static var lc_chatNewGroupTitle : String {
        get {
            return "chatNewGroupTitle".localized()
        }} 

    static var lc_chatGroupNamePlaceholderText : String {
        get {
            return "chatGroupNamePlaceholderText".localized()
        }} 

    static var lc_chatCreateGroupSuccesMessage : String {
        get {
            return "chatCreateGroupSuccesMessage".localized()
        }} 

    static var lc_chatCreateGroupErrorMessage : String {
        get {
            return "chatCreateGroupErrorMessage".localized()
        }} 

    static var lc_chatNoMessgaes : String {
        get {
            return "chatNoMessgaes".localized()
        }} 

    static var lc_chatSupportTitle : String {
        get {
            return "chatSupportTitle".localized()
        }} 

    static var lc_chatMessageStatusDelivered : String {
        get {
            return "chatMessageStatusDelivered".localized()
        }} 

    static var lc_chatMessageStatusRead : String {
        get {
            return "chatMessageStatusRead".localized()
        }} 

    static var lc_chatAddUserButtonTitle : String {
        get {
            return "chatAddUserButtonTitle".localized()
        }} 

    static var lc_chatExitFromGroupTitle : String {
        get {
            return "chatExitFromGroupTitle".localized()
        }} 

    static var lc_chatMembersCount : String {
        get {
            return "chatMembersCount".localized()
        }} 

    static var lc_payCantPayError : String {
        get {
            return "payCantPayError".localized()
        }} 


//***************************************************
    func localized() -> String {
        return String.translation("Localizable", self)
   }
   
    static func translation(_ table: String, _ key: String, _ args: CVarArg...) -> String {
       
       let format = NSLocalizedString(key, tableName: table, bundle: Bundle(for: BundleToken.self), comment: "")
       return String(format: format, locale: Locale.current, arguments: args)
   }
}

private final class BundleToken {}

//***************************************************
 

