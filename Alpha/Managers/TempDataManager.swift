//
//  TempDataManager.swift
//  Alpha
//
//  Created by alexandr shitikov on 06.10.2020.
//  Copyright © 2020 alexandr shitikov. All rights reserved.
//

import Foundation
import UIKit

protocol TempDataManagerDelegate: class {
    func visitedEventsListDidCahnge(items:[ALProfile.EventsResponseElement])
}

extension TempDataManagerDelegate {
    func visitedEventsListDidCahnge(items:[ALProfile.EventsResponseElement]) {}
}


class TempDataManager {
    static let shared = TempDataManager() 
    private var photos = [PhotoCash]()
    private var inDownloading = [String]()
    private var visitedEvents = [ALProfile.EventsResponseElement]()
    weak var delegate:TempDataManagerDelegate?
    
    private init() {
        
    }
    
    func getPhoto(url:String?, completion:@escaping(UIImage?) -> Void) {
        let exist = inDownloading.first(where: {$0 == url})
        if let photo = photos.first(where: {$0.url == url}) {
            completion(photo.image)
        } else if exist == nil {
            self.inDownloading.append(url ?? "")
            APIManager.manager.getPhoto(url: url) { (image) in
                if let photo = image {
                    let newPhoto = PhotoCash(image: photo, url: url)
                    self.photos.append(newPhoto)
                    completion(image)
                } else {
                    completion(nil)
                }
            }
        }
    }
    
    func getVisitedEvents() -> [ALProfile.EventsResponseElement] {
        return visitedEvents
    }
    
    func saveVisitedEvents(events:ALProfile.EventsResponse) {
        visitedEvents.removeAll()
        visitedEvents = events
        delegate?.visitedEventsListDidCahnge(items: visitedEvents)
    }
    
}

struct PhotoCash {
    let image:UIImage?
    let url:String?
}
