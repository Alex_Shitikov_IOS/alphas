import UIKit
import TwilioChatClient

protocol MessagingManagerDelegate: class {
    func synchronizationStatusUpdated()
}

class MessagingManager: NSObject {
    
    static let _sharedManager = MessagingManager()
    var client:TwilioChatClient?
    var delegate:ChannelManager?
    var connected = false
    var messagingDelegate:MessagingManagerDelegate?
    
    var userIdentity:String {
        return SessionManager.getUsername()
    }
    
    var hasIdentity: Bool {
        return SessionManager.isLoggedIn()
    }
    
    override init() {
        super.init()
        delegate = ChannelManager.sharedManager
    }
    
    class func sharedManager() -> MessagingManager {
        return _sharedManager
    }
    
    func joinChannel(channel: TCHChannel?, completion: @escaping (Bool) -> Void) {
        if let newChannel = channel {
            if let _ = newChannel.members?.membersList().first(where: {$0.identity == client?.user?.identity}) {
                completion(true)
                return
            }
            newChannel.join { (channelResult) in
                if channelResult.isSuccessful() {
                    print("Channel joined.")
                    completion(true)
                } else {
                    print("Channel NOT joined.")
                    completion(true)
                }
            }
        }
        
    }
    
    // MARK: User and session management
    
    func loginWithUsername(username: String,
                           completion: @escaping (Bool, NSError?) -> Void) {
        SessionManager.loginWithUsername(username: username)
        connectClientWithCompletion(completion: completion)
    }
    
    func logout() {
        SessionManager.logout()
        DispatchQueue.global(qos: .userInitiated).async {
            self.client?.shutdown()
            self.client = nil
        }
        self.connected = false
    }
    
    // MARK: Twilio Client
    
    func loadGeneralChatRoomWithCompletion(completion:@escaping (Bool, NSError?) -> Void) {
        ChannelManager.sharedManager.joinGeneralChatRoomWithCompletion { succeeded in
            if succeeded {
                completion(succeeded, nil)
            }
            else {
                let error = self.errorWithDescription(description: "Could not join General channel", code: 300)
                completion(succeeded, error)
            }
        }
    }
    
    func connectClientWithCompletion(completion: @escaping (Bool, NSError?) -> Void) {
        if (client != nil) {
            logout()
        }
        
        requestTokenWithCompletion { succeeded, token in
            if let token = token, succeeded {
                self.initializeClientWithToken(token: token)
                completion(succeeded, nil)
            }
            else {
                let error = self.errorWithDescription(description: "Could not get access token", code:301)
                completion(succeeded, error)
            }
        }
    }
    
    func initializeClientWithToken(token: String) {
        TwilioChatClient.chatClient(withToken: token, properties: nil, delegate: self) { [weak self] result, chatClient in
            guard (result.isSuccessful()) else { return }
            
            
            self?.connected = true
            self?.client = chatClient
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            chatClient?.register(withNotificationToken: appDelegate.updatedPushToken ?? Data(), completion: { (result) in
                print("result tov register token -> \(result)")
            })
        }
    }
    
    func requestTokenWithCompletion(completion:@escaping (Bool, String?) -> Void) {
        if let _ = UIDevice.current.identifierForVendor?.uuidString {
            let url = PCURLs.Twilio.token.rawValue
            APIManager.manager.performRequestWith(method: .GET, url: url, params: nil, showLoader: false, completion: { (data, response, error) in
                let item = data?.parseCodableObject(structType: TwilioTokenResponse.self)
                completion(item?.token != nil, item?.token)
            })
        }
    }
    
    func errorWithDescription(description: String, code: Int) -> NSError {
        let userInfo = [NSLocalizedDescriptionKey : description]
        return NSError(domain: "app", code: code, userInfo: userInfo)
    }
}



// MARK: - TwilioChatClientDelegate
extension MessagingManager : TwilioChatClientDelegate {
    func chatClient(_ client: TwilioChatClient, channelAdded channel: TCHChannel) {
        self.delegate?.chatClient(client, channelAdded: channel)
    }
    
    func chatClient(_ client: TwilioChatClient, channel: TCHChannel, updated: TCHChannelUpdate) {
        self.delegate?.chatClient(client, channel: channel, updated: updated)
    }
    
    func chatClient(_ client: TwilioChatClient, channelDeleted channel: TCHChannel) {
        self.delegate?.chatClient(client, channelDeleted: channel)
    }
    
    func chatClient(_ client: TwilioChatClient, synchronizationStatusUpdated status: TCHClientSynchronizationStatus) {
        if status == TCHClientSynchronizationStatus.completed {
            ChannelManager.sharedManager.channelsList = client.channelsList()
            ChannelManager.sharedManager.populateChannelDescriptors()
            loadGeneralChatRoomWithCompletion { success, error in
                if success {
                    self.messagingDelegate?.synchronizationStatusUpdated()
                }
            }
        }
        self.delegate?.chatClient(client, synchronizationStatusUpdated: status)
    }
    
    func chatClientTokenWillExpire(_ client: TwilioChatClient) {
        requestTokenWithCompletion { succeeded, token in
            if (succeeded) {
                client.updateToken(token!)
            }
            else {
                print("Error while trying to get new access token")
            }
        }
    }
    
    func chatClientTokenExpired(_ client: TwilioChatClient) {
        requestTokenWithCompletion { succeeded, token in
            if (succeeded) {
                client.updateToken(token!)
            }
            else {
                print("Error while trying to get new access token")
            }
        }
    }
}

struct TwilioTokenResponse: Codable {
    let token:String
}

