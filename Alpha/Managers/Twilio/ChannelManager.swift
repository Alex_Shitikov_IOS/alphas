import UIKit
import TwilioChatClient

protocol ChannelManagerDelegate {
    func reloadChannelDescriptorList()
}

class ChannelManager: NSObject {
    static let sharedManager = ChannelManager()
    
    static let defaultChannelUniqueName = "general"
    static let defaultChannelName = "GENERAL_FGHNBVGTHSER"
    let workGroup = DispatchGroup()
    
    var supportManager: SupportManager?
    var delegate: ChannelManagerDelegate?
    
    var channelsList: TCHChannels?
    var channelDescriptors: NSOrderedSet?
    var generalChannel: TCHChannel!
    var users = [TCHUserDescriptor]()
    
    override init() {
        super.init()
        channelDescriptors = NSMutableOrderedSet()
    }
    
    // MARK: - General channel
    
    func joinGeneralChatRoomWithCompletion(completion: @escaping (Bool) -> Void) {
        
        let uniqueName = ChannelManager.defaultChannelUniqueName
        if let channelsList = self.channelsList {
            channelsList.channel(withSidOrUniqueName: uniqueName) { result, channel in
                self.generalChannel = channel
                
                if self.generalChannel != nil {
                    self.joinGeneralChatRoomWithUniqueName(name: nil, completion: completion)
                } else {
                    self.createGeneralChatRoomWithCompletion { succeeded in
                        if (succeeded) {
                            self.joinGeneralChatRoomWithUniqueName(name: uniqueName, completion: completion)
                            return
                        }
                        
                        completion(false)
                    }
                }
            }
        }
    }
    
    
    
    
    func joinGeneralChatRoomWithUniqueName(name: String?, completion: @escaping (Bool) -> Void) {
        generalChannel.join { result in
            if ((result.isSuccessful()) && name != nil) {
                self.setGeneralChatRoomUniqueNameWithCompletion(completion: completion)
                return
            }
            completion((result.isSuccessful()))
        }
    }
    
    func getUsersFromGeneralChannel() {
        users.removeAll()
        generalChannel.members?.membersList().forEach({ (member) in
            member.userDescriptor { (result, descriptor) in
                if let userInfo = descriptor {
                    self.users.append(userInfo)
                }
            }
        })
    }
    
    func createGeneralChatRoomWithCompletion(completion: @escaping (Bool) -> Void) {
        let channelName = ChannelManager.defaultChannelName
        let options = [
            TCHChannelOptionFriendlyName: channelName,
            TCHChannelOptionType: TCHChannelType.public.rawValue,
            ChannelInfoAttrtibutes.CodingKeys.type.rawValue : ChannelType.group.rawValue
        ] as [String : Any]
        channelsList!.createChannel(options: options) { result, channel in
            if (result.isSuccessful()) {
                self.generalChannel = channel
            }
            completion((result.isSuccessful()))
        }
    }
    
    func setGeneralChatRoomUniqueNameWithCompletion(completion:@escaping (Bool) -> Void) {
        generalChannel.setUniqueName(ChannelManager.defaultChannelUniqueName) { result in
            completion((result.isSuccessful()))
        }
    }
    
    // MARK: - Populate channel Descriptors
    
    func populateChannelDescriptors() {
        channelsList?.userChannelDescriptors { result, paginator in
            guard let paginator = paginator else {
                return
            }
            
            let newChannelDescriptors = NSMutableOrderedSet()
            newChannelDescriptors.addObjects(from: paginator.items())
            self.channelsList?.publicChannelDescriptors { result, paginator in
                guard let paginator = paginator else {
                    return
                }
                
                let channelIds = NSMutableSet()
                for descriptor in newChannelDescriptors {
                    if let descriptor = descriptor as? TCHChannelDescriptor {
                        if let sid = descriptor.sid {
                            channelIds.add(sid)
                        }
                    }
                }
                for descriptor in paginator.items() {
                    if let sid = descriptor.sid {
                        if !channelIds.contains(sid) {
                            channelIds.add(sid)
                            newChannelDescriptors.add(descriptor)
                        }
                    }
                }
                
                // sort the descriptors
                let sortSelector = #selector(NSString.localizedCaseInsensitiveCompare(_:))
                let descriptor = NSSortDescriptor(key: "friendlyName", ascending: true, selector: sortSelector)
                newChannelDescriptors.sort(using: [descriptor])
                
                self.channelDescriptors = newChannelDescriptors
                
                if let delegate = self.delegate {
                    self.getUsersFromGeneralChannel()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                        delegate.reloadChannelDescriptorList()
                    }
                }
            }
        }
    }
    
    func chatWithUser(userIdentity:String, completion:@escaping(TCHChannel?) -> Void) {
        var resultDescriptor:TCHChannelDescriptor?
        let channels = ChannelManager.sharedManager.channelDescriptors
        channels?.forEach({ (channelDescription) in
            let description = channelDescription as? TCHChannelDescriptor
            let attributes = ChannelInfoAttrtibutes(attributes: description?.attributes()?.dictionary)
            if attributes.type == ChannelType.user {
                if description?.createdBy == userIdentity || attributes.userId == userIdentity {
                    resultDescriptor = description
                    return
                }
            }
        })
        
        if resultDescriptor == nil {
            print("crate channel")
            self.createChannelWithName(name: "\(Date())", usersIdentity: [userIdentity], channelType: .user, isPrivate: true) { (result, newChannel) in
                completion(newChannel)
                return
            }
        } else {
            resultDescriptor?.channel(completion: { (result, channel) in
                completion(channel)
            })
        }
    }
    
    func chatWithSupport(userIdentity:String, completion:@escaping(TCHChannel?) -> Void) {
        var resultDescriptor:TCHChannelDescriptor?
        let channels = ChannelManager.sharedManager.channelDescriptors
        channels?.forEach({ (channelDescription) in
            let description = channelDescription as? TCHChannelDescriptor
            let attributes = ChannelInfoAttrtibutes(attributes: description?.attributes()?.dictionary)
            if attributes.type == ChannelType.support {
                resultDescriptor = description
                resultDescriptor?.channel(completion: { (result, channel) in
                    channel?.members?.add(byIdentity: userIdentity,
                                          completion: { (result) in
                                            print(result)
                                          })
                })
                return
            }
        })
        
        if resultDescriptor == nil {
            print("crate support channel")
            self.createChannelWithName(name: "Support_", usersIdentity: [userIdentity], channelType: .support, isPrivate: true) { (result, newChannel) in
                completion(newChannel)
                return
            }
        } else {
            resultDescriptor?.channel(completion: { (result, channel) in
                completion(channel)
            })
        }
    }
    
    
    // MARK: - Create channel
    
    func createChannelWithName(name: String, usersIdentity:[String], channelType:ChannelType, isPrivate:Bool, completion: @escaping (Bool, TCHChannel?) -> Void) {
        if (name == ChannelManager.defaultChannelName) {
            completion(false, nil)
            return
        }
        
        let channelOptions = [
            TCHChannelOptionFriendlyName: name,
            TCHChannelOptionType: isPrivate == true ? TCHChannelType.private.rawValue : TCHChannelType.public.rawValue,
        ] as [String : Any]
        self.channelsList?.createChannel(options: channelOptions) { result, channel in
            if result.isSuccessful() {
                if channelType == .user {
                    let attributes: TCHJsonAttributes = TCHJsonAttributes(dictionary:
                                                                            [ChannelInfoAttrtibutes.CodingKeys.type.rawValue: channelType.rawValue,
                                                                             ChannelInfoAttrtibutes.CodingKeys.userId.rawValue: usersIdentity.first ?? ""])
                    channel?.setAttributes(attributes, completion: { (result) in
                        print(result)
                    })
                } else if channelType == .group {
                    let attributes: TCHJsonAttributes = TCHJsonAttributes(dictionary:
                                                                            [ChannelInfoAttrtibutes.CodingKeys.type.rawValue: channelType.rawValue])
                    channel?.setAttributes(attributes, completion: { (result) in
                        print(result)
                    })
                } else {
                    let attributes: TCHJsonAttributes = TCHJsonAttributes(dictionary:
                                                                            [ChannelInfoAttrtibutes.CodingKeys.type.rawValue: channelType.rawValue])
                    channel?.setAttributes(attributes, completion: { (result) in
                        print(result)
                    })
                }
                
                
                usersIdentity.forEach { (user) in
                    channel?.members?.add(byIdentity: user, completion: { (result) in
                        print("result to added user to channel \(result.isSuccessful())")
                    })
                }
            }
            if result.isSuccessful() {
                MessagingManager.sharedManager().joinChannel(channel: channel) { (joinResult) in
                    
                }
            }
            completion((result.isSuccessful()), channel)
        }
    }
    
    func leaveCahnnel(channelSid:String) {
        channelsList?.channel(withSidOrUniqueName: channelSid, completion: { (result, channel) in
            channel?.leave(completion: { (result) in
                if result.isSuccessful() {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                        self.populateChannelDescriptors()
                    }
                }
                print(result)
            })
            
        })
    }
    
}

// MARK: - TwilioChatClientDelegate

extension ChannelManager : TwilioChatClientDelegate {
    func chatClient(_ client: TwilioChatClient, channelAdded channel: TCHChannel) {
        DispatchQueue.main.async {
            self.populateChannelDescriptors()
        }
    }
    
    func chatClient(_ client: TwilioChatClient, channel: TCHChannel, updated: TCHChannelUpdate) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            //self.delegate?.reloadChannelDescriptorList()
            self.populateChannelDescriptors()
        }
    }
    
    func chatClient(_ client: TwilioChatClient, channelDeleted channel: TCHChannel) {
        DispatchQueue.main.async {
            self.populateChannelDescriptors()
        }
        
    }
    
    func chatClient(_ client: TwilioChatClient, synchronizationStatusUpdated status: TCHClientSynchronizationStatus) {
    }
    
    //MARK: Helpers
    
    func getChannelTitle(channel:TCHChannelDescriptor?) -> String? {
        let attribures = ChannelInfoAttrtibutes(attributes: channel?.attributes()?.dictionary)
        if attribures.type == ChannelType.user {
            if channel?.createdBy == MessagingManager.sharedManager().client?.user?.identity {
                let user = ChannelManager.sharedManager.users.first(where: {$0.identity == attribures.userId})
                return user?.friendlyName
            } else {
                let user = ChannelManager.sharedManager.users.first(where: {$0.identity == channel?.createdBy})
                return user?.friendlyName
            }
        } else if attribures.type == ChannelType.support{
            let user = ChannelManager.sharedManager.users.first(where: {$0.identity == channel?.createdBy})
            return user?.friendlyName
        }
        
        return channel?.friendlyName
    }
    
    func getAvatar(channel:TCHChannelDescriptor?) -> String? {
        let attribures = ChannelInfoAttrtibutes(attributes: channel?.attributes()?.dictionary)
        if attribures.type == ChannelType.user {
            if channel?.createdBy == MessagingManager.sharedManager().client?.user?.identity {
                let user = ChannelManager.sharedManager.users.first(where: {$0.identity == attribures.userId})
                let userAttributes = UserInfoAttributes(attributes: user?.attributes()?.dictionary)
                return userAttributes.photoUrl
            } else {
                let user = ChannelManager.sharedManager.users.first(where: {$0.identity == channel?.createdBy})
                let userAttributes = UserInfoAttributes(attributes: user?.attributes()?.dictionary)
                return userAttributes.photoUrl
            }
        } else if attribures.type == ChannelType.support {
            let user = ChannelManager.sharedManager.users.first(where: {$0.identity == channel?.createdBy})
            let userAttributes = UserInfoAttributes(attributes: user?.attributes()?.dictionary)
            return userAttributes.photoUrl
        }
        
        return attribures.photo
    }
}
