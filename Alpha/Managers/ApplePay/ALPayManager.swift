//
//  ALPayManager.swift
//  Alpha
//
//  Created by alexandr shitikov on 26.10.2020.
//  Copyright © 2020 alexandr shitikov. All rights reserved.
//

import Foundation
import UIKit
import PassKit


class ALPayManager {
    
    static let shared = ALPayManager()
    private init() {}
    
    func buy(item:PayItem, target:UIViewController) {
        let price = NSDecimalNumber(value: item.price)
        let paymentItem = PKPaymentSummaryItem(label: item.title, amount: price)
        let paymentNetworks = [PKPaymentNetwork.amex, .discover, .masterCard, .visa]
        
        if PKPaymentAuthorizationViewController.canMakePayments(usingNetworks: paymentNetworks) {
            
            let request = PKPaymentRequest()
            request.currencyCode = "UAH"
            request.countryCode = "UA"
            request.merchantIdentifier = "******"
            request.merchantCapabilities = PKMerchantCapability.capability3DS
            request.supportedNetworks = paymentNetworks
            request.paymentSummaryItems = [paymentItem]
            guard let paymentController = PKPaymentAuthorizationViewController(paymentRequest: request)
            else {
                cantPayAlert(target: target)
                return
            }
            paymentController.delegate = target as! PKPaymentAuthorizationViewControllerDelegate
            target.present(paymentController, animated: true, completion: nil)
            
        } else {
            cantPayAlert(target: target)
        }
    }
    
    private func cantPayAlert(target:UIViewController) {
        let data = AlertData(title: String.lc_payCantPayError, message: nil, type: .error)
        ALAlertViewController.show(data: data, target: target)
    }
}

struct PayItem {
    let title:String
    let price:Double
    let count:Int
}



