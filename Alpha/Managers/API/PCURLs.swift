//
//  PCURLs.swift
//  Pcounter
//
//  Created by alexandr shitikov on 11.02.2020.
//  Copyright © 2020 Intranso. All rights reserved.
//

import Foundation

enum PCURLs {
    
    enum User: String {
        case registerationGeneral = "api/client/registration/general" //POST
        case registerationPassword = "api/client/registration/password" //POST
        case registrationDetails = "api/client/registration/details" //POST
        case availableInterests = "api/client/available-interests" //GET
        case login = "api/client/login" //POST
        case currentUser = "api/client/current" //GET
    }
    
    enum Password: String {
        case forgot = "api/forgot-password"
        case validate = "api/forgot-password/validate"
        case reset = "api/forgot-password/reset"
    }
    
    enum Events: String {
        case visited = "api/event/visited"
    }
    
    enum File: String {
        case upload = "api/file/upload"
    }
    
    enum Token: String {
        case registerFCM = "api/token"
    }
    
    enum Twilio: String {
        case token = "api/chat/twillio/token"
        case manager = "api/chat/manager"
    }

    enum Gallary: String {
        case eventPhotos = "api/event/gallery"
    }
    

    
   
  
}
