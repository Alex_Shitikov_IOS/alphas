//
//  APIManager.swift
//
//  Created by alexandr shitikov on 19.07.2019.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.
//

import Foundation
import UIKit
import JWTDecode


enum HTTPMethod: String {
    case DELETE = "DELETE"
    case GET    = "GET"
    case HEAD   = "HEAD"
    case POST   = "POST"
    case PUT    = "PUT"
    case PATCH  = "PATCH"
}

enum ResourceType {
    case oauth
    case resources
    case noCredentials
    case images
    case noBaseUrl
}

enum Server: String {
    case development
    case stage
    case production
}

struct LoginModel: Codable {
    let username: String
    let password: String
    var grant_type = "***"
}

fileprivate let STAGE_OAUTH_URL = "***"
fileprivate let DEV_OAUTH_URL   = "***"
fileprivate let PROD_OAUTH_URL  = "***"

fileprivate let STAGE_RESOURCE_URL = "***"
fileprivate let DEV_RESOURCE_URL   = "***"
fileprivate let PROD_RESOURCE_URL  = "***"


var ENVIRONMENT: Server = .development

let BASE_URL = ENVIRONMENT == .stage ? STAGE_OAUTH_URL : ENVIRONMENT == .development ? DEV_OAUTH_URL : PROD_OAUTH_URL
fileprivate let RESOURCE_URL = ENVIRONMENT == .stage ? STAGE_RESOURCE_URL : ENVIRONMENT == .development ? DEV_RESOURCE_URL : PROD_RESOURCE_URL

fileprivate let CLIENT_SECRET = "****"

fileprivate let key = "****"

struct LoginCredentials: Codable {
    let token: String

    func isExpired() -> Bool {
        return false
    }
    
    mutating func storeCredentials() {
      
    }
    
    static func  getCredentials() -> LoginCredentials? {
        return LoginCredentials(token: "")
    }
}

//**********************************************************************
class APIManager {
    
    private let session: URLSession
    static let manager = APIManager()
    private let encodedSecret = CLIENT_SECRET
    let reachability: Reachability
    private var savedRequests: [DispatchWorkItem] = [DispatchWorkItem]()
    private var isTokenRefreshing: Bool = false
    var currentUser:ALProfile.Response?
    
    private init() {
        self.session = URLSession(configuration: .default)
        self.reachability = Reachability()!
        do {
            try reachability.startNotifier()
        } catch {
            print("REACHABILITY : could not start reachability notifier!")
        }
    }
    
    //MARK: - Check user -
    func userAuthorized(completion: @escaping (Bool) -> Void) {
        guard let credentials = LoginCredentials.getCredentials() else {
            completion(false)
            return
        }
        if credentials.isExpired() {
            refreshToken { [weak self] (error, success) in
                if success == false {
                    completion(false)
                    self?.logout()
                } else {
                    completion(true)
                }
            }
        } else {
            completion(true)
        }
    }
    
    func cancelAllTasks() {
        session.invalidateAndCancel()
    }
    
    //MARK: - Headers configurator -
    fileprivate func setHeadersFor(request: inout URLRequest, resource: ResourceType) {
        switch resource {
        case .resources:
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("Bearer " + ((LoginCredentials.getCredentials()?.token) ?? ""), forHTTPHeaderField: "Authorization")
        case .oauth:
            request.setValue("Basic " + encodedSecret, forHTTPHeaderField: "Authorization")
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpMethod = HTTPMethod.POST.rawValue
        case .noCredentials:
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        case .images:
            request.httpMethod = HTTPMethod.POST.rawValue
            request.setValue("Bearer " + ((LoginCredentials.getCredentials()?.token) ?? ""), forHTTPHeaderField: "Authorization")
        case .noBaseUrl:
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        }
    }
    
    //MARK: - Login/refresh token -
    func loginUser(request:ALLogin.Request, completion: @escaping (ALResponseError?, URLResponse?, Bool) -> Void) {
        guard reachability.connection != .none else {
            print("No Internet Connection...")
            hideLoader(loader: true)
            let serverError = ALResponseError(message: "No Internet Connection...")
            completion(serverError, nil, false)
            return
        }
        let url = PCURLs.User.login.rawValue
        performRequestWith(method: .POST, url: url, params: request.dictionary, showLoader: false, completion: { (data, response, respError) in
            if response?.isSuccess() == true {
                guard let responseData = data else {
                    DispatchQueue.main.async {
                        completion(nil, response, false)
                    }
                    return
                }
                do {
                    var result = try JSONDecoder().decode(LoginCredentials.self, from: responseData)
                    result.storeCredentials()
                    DispatchQueue.main.async {
                        completion(nil, response, true)
                    }
                } catch {
                    DispatchQueue.main.async {
                        completion(respError, response, false)
                    }
                }
            } else {
                DispatchQueue.main.async {
                    completion(respError, response, false)
                }
            }
            
        }, resourceType: .noCredentials)
    }
    
    func getPhoto(url:String?, withBaseUrl:Bool = false, completion: @escaping (UIImage?) -> Void) {
        if let imageUrl = url {
            APIManager.manager.performRequestWith(method: .GET, url: imageUrl, params: nil, showLoader: false, completion: { (data, response, error) in
                if let dataImage = data {
                    completion(UIImage(data: dataImage))
                } else {
                    completion(nil)
                }
            }, resourceType: withBaseUrl == true ? .resources : .noBaseUrl)
        }
        
    }

private func saveRequest(_ block: @escaping () -> Void) {
    // Save request to DispatchWorkItem array
    savedRequests.append( DispatchWorkItem {
        block()
    })
}

private func executeAllSavedRequests() {
    savedRequests.forEach({ DispatchQueue.global().async(execute: $0) })
    savedRequests.removeAll()
}

private func refreshToken(completion: @escaping (Error?, Bool) -> Void) {
    guard let credentials = LoginCredentials.getCredentials() else {
        completion(nil, false)
        return
    }
    
    //Build url request for refresh token
    var request = URLRequest(url: URL(string: BASE_URL + "?grant_type=refresh_token&refresh_token=" + credentials.token)!)
    print(request.debugDescription)
    setHeadersFor(request: &request, resource: .oauth)
    
    let task = session.dataTask(with: request) { (data, response, error) in
        print(response.debugDescription)
        guard let responseData = data else {
            DispatchQueue.main.async {
                completion(error, false)
            }
            return
        }
        if response?.isSuccess() == true {
            do {
                var result = try JSONDecoder().decode(LoginCredentials.self, from: responseData)
                result.storeCredentials()
                DispatchQueue.main.async {
                    completion(nil, true)
                }
            } catch {
                DispatchQueue.main.async {
                    completion(error, false)
                }
            }
        } else {
            DispatchQueue.main.async {
                completion(error, false)
            }
        }
    }
    task.resume()
}

//MARK: - Access to protected resources -
    
func performRequestWith(method: HTTPMethod, url: String, params: Any? = nil, showLoader: Bool = false, completion:@escaping (Data?, URLResponse?, ALResponseError?) -> Void, resourceType: ResourceType = .resources) {
    guard reachability.connection != .none else {
        print("No Internet Connection...")
        return
    }
    
    if resourceType == .noCredentials {
        self.performRequest(method: method, url: url, params: params, completion: { (data, response, error) in
            self.hideLoader(loader:showLoader)
            DispatchQueue.main.async {
                completion(data, response, error)
            }
        }, resourceType: resourceType)
        return
    }
    
    if isTokenRefreshing {
        saveRequest {
            self.performRequestWith(method: method, url: url, params: params, showLoader: showLoader, completion: completion, resourceType: resourceType)
        }
        return
    }
    
    guard let credentials = LoginCredentials.getCredentials() else {
        completion(nil, nil, nil)
        return
    }
    if credentials.isExpired() {
        // open this flag for we need wait refresh token
        isTokenRefreshing = true
        
        saveRequest {
            self.performRequestWith(method: method, url: url, params: params, showLoader: showLoader, completion: completion, resourceType: resourceType)
        }
        refreshToken { (error, success)  in
            self.isTokenRefreshing = false
            self.executeAllSavedRequests()
        }
    } else {
        self.performRequest(method: method, url: url, params: params, completion: { (data, response, error) in
            self.hideLoader(loader:showLoader)
            DispatchQueue.main.async {
                completion(data, response, error)
            }
        }, resourceType: resourceType)
    }
}

fileprivate func performRequest(method: HTTPMethod, url: String, params: Any?, completion:@escaping (Data?, URLResponse?, ALResponseError?) -> Void, resourceType: ResourceType = .resources) {
    //        NotificationCenter.default.post(name: PCNetworkNotification.PCNetworkStarLoad.Name, object: nil)
    var request = URLRequest(url: URL(string: RESOURCE_URL + url)!)
    if resourceType == .noBaseUrl {
        request = URLRequest(url:URL(string:url) ?? URL(string: RESOURCE_URL)!)
    }
    print("-------------------------------------------------------------")
    print("\(request.debugDescription) ")
    print("-------------------------------------------------------------")
    
    request.httpMethod = method.rawValue
    
    setHeadersFor(request: &request, resource: resourceType)
    
    if let paramsObject = params {
        if let parameter = params as? String {
            request.httpBody = parameter.data(using: String.Encoding.utf8)
        } else {
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: paramsObject, options: .prettyPrinted)
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    let task = session.dataTask(with: request) { (data, response, error) in
        if (response as? HTTPURLResponse)?.statusCode == 401 || (response as? HTTPURLResponse)?.statusCode ==  403 {
            DispatchQueue.main.async {
                if AppDelegate.topViewController() is ALLoginViewController {
                    print("you are in login scene")
                } else {
                    self.logout()
                }
            }
            
        }
        
        if let serverError = data?.parseCodableObject(structType: ALResponseError.self) {
            completion(nil, response, serverError)
        } else {
            completion(data, response, nil)
        }
    }
    task.resume()
}
    
    //Upload image

    func uploadImage(url: String, params: [String: String]?, image: UIImage, completion: @escaping (Data?, URLResponse?, ALResponseError?) -> Void) {
        let cropedImage = image.resizeImage(newWidth: 320)
        
        guard let imageData = (cropedImage ?? image).fixImageOrientation().jpegData(compressionQuality: 0.3) else {
            let error = ALResponseError(message: "Can't proced with image")
            completion(nil, nil, error)
            return
        }
        let boundary = generateBoundaryString()
        var request = URLRequest(url: URL(string: RESOURCE_URL + url)!)
        setHeadersFor(request: &request, resource: .images)
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        request.httpBody = createBodyWithParameters(parameters: params, filePathKey: "file", imageDataKey: imageData, boundary: boundary)
        let task = session.dataTask(with: request) { (data, response, error) in
            let serverError = data?.parseCodableObject(structType: ALResponseError.self)
            completion(data, response, serverError)
        }
        task.resume()
    }
    
    private func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, imageDataKey: Data, boundary: String) -> Data {
        var body = Data();
        if parameters != nil {
            for (key, value) in parameters! {
                body.append(Data("--\(boundary)\r\n".utf8))
                body.append(Data("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".utf8))
                body.append(Data("\(value)\r\n".utf8))
            }
        }
        let filename = "image.jpg"
        let mimetype = "image/jpg"
        body.append(Data("--\(boundary)\r\n".utf8))
        body.append(Data("Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n".utf8))
        body.append(Data("Content-Type: \(mimetype)\r\n\r\n".utf8))
        body.append(imageDataKey)
        body.append(Data("\r\n".utf8))
        body.append(Data("--\(boundary)--\r\n".utf8))
        return body
    }
    
    func generateBoundaryString() -> String {
        return "Boundary-\(UUID().uuidString)"
    }
    
    
    

//MARK: - Logout -
func logout() {
    UserDefaults.standard.set(nil, forKey: key)
    DispatchQueue.main.async {
        UIApplication.shared.applicationIconBadgeNumber = 0
        UIApplication.shared.keyWindow?.rootViewController?.dismiss(animated: true, completion: nil)
    }
}

//Loader support
private func showLoaderIfNeeded(_ flag: Bool) {
    if flag {
        //#warning("loader start - not implemented")
    }
}

private func hideLoader(loader:Bool) {
    if loader {
        //#warning("loader stop - not implemented")
    }
}
}
//****************
struct AtachedFile {
    let fileName: String
    let mediaType: String
    let data: Data
}

// MARK: - RegistrationResponse
struct RegistrationResponse: Codable {
    let token: String
    
    enum CodingKeys: String, CodingKey {
        case token = "token"
    }
}

struct ALResponseError:Codable {
    let message:String
}


