//
//  ALTextInput.swift
//  Alpha
//
//  Created by alexandr shitikov on 08.09.2020.
//  Copyright © 2020 alexandr shitikov. All rights reserved.
//

import UIKit
//"^[0-9]{10}$" phone
fileprivate let emailRegex    = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
fileprivate let passwordRegex = "^.{6,}$"
fileprivate let phoneRegex    = "^.{13,}$"
fileprivate let textRegex     = "^.{1,}$"
fileprivate let codeRegex = "^.{6}$"



enum TextInputHeight:CGFloat {
    case normal = 80
    case error = 90
}

enum ALTextInputType {
    case name
    case lastName
    case email
    case phone
    case createPassword
    case confirmPassword
    case about
    case login
    case password
    case code
}

enum ALTextInputState: CGFloat {
    case normal = 80.00001
    case input = 80
    case error = 90
    

}

struct TextInputItem {
    let type: ALTextInputType
    let state: ALTextInputState
    let message:String?
    let inputHeight:TextInputHeight
}

protocol ALTextInputDelegate: class {
    func textDidCahnge(item: TextInputItem)
}

class ALTextInput: UIView, UITextFieldDelegate {
    
    private var inputType: ALTextInputType = .name
    private var state: ALTextInputState?

    @IBOutlet var mainView: UIView!
    @IBOutlet weak var textFieldView: UIView!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var rightButton: UIButton!
    @IBOutlet weak var textField: UITextField!
    
    weak var delegate:ALTextInputDelegate?
    var animationTime:TimeInterval = 1.0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("ALTextInput", owner: self, options: nil)
        addSubview(mainView)
        mainView.frame = self.bounds
        mainView.clipsToBounds = true
        mainView.backgroundColor = .clear
        textFieldView.backgroundColor = .inputBackground
        textFieldView.rounded(radius: 18)
        textField.placeholderColor = .inputPlaceholder
        rightButton.isHidden = true
        errorLabel.textColor = .inputError
        setState(state: .normal)
        textField.delegate = self
        textField.textColor = .mainTextBlack
        translatesAutoresizingMaskIntoConstraints = false
    }
    
    func setViewType(type:ALTextInputType, tag:Int) {
        textField.tag = tag
        self.inputType = type
        switch type {
        case .name:
            textField.placeholder = String.lc_inputName
        case .lastName:
            textField.placeholder = String.lc_inputLastName
        case .email:
            textField.placeholder = String.lc_inputEmail
            textField.keyboardType = .emailAddress
        case .phone:
            textField.placeholder = String.lc_inputPhone
            textField.keyboardType = .numbersAndPunctuation
        case .createPassword:
            textField.placeholder = String.lc_inputPasswordCreate
            textField.isSecureTextEntry = true
        case .confirmPassword:
            textField.placeholder = String.lc_inputConfirmPassword
            textField.isSecureTextEntry = true
        case .about:
            textField.placeholder = String.lc_inputAbout
        case .login:
            textField.placeholder = String.lc_inputPhone
            textField.keyboardType = .numbersAndPunctuation
        case .password:
            textField.placeholder = String.lc_loginScenePlaceholderPassword
            textField.isSecureTextEntry = true
        case .code:
            textField.placeholder = String.lc_forgotEnterCode
            textField.textAlignment = .center
        }
    }
    
    func setState(state:ALTextInputState) {
         DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            switch state {
            case .normal:
                self.textFieldView.setBorderGradient(colors: [.white], cornerRadius: 16)
                self.errorLabel.text = ""
                self.errorLabel.isHidden = true
            case .input:
                self.textFieldView.setBorderGradient(colors: [.orangeButton, .lightOrangeButton], cornerRadius: 16, animation: true)
                self.errorLabel.text = ""
                self.errorLabel.isHidden = true
            case .error:
                self.textFieldView.setBorderGradient(colors: [.inputError, .inputError], cornerRadius: 16)
                self.errorLabel.isHidden = false
            }
        }
       

    }
    
    func isValid() -> Bool {
        var regex = textRegex
        switch inputType {
        case .login:
            regex = phoneRegex
        case .name:
            regex = textRegex
        case .lastName:
            regex = textRegex
        case .email:
            regex = emailRegex
        case .phone:
            regex = phoneRegex
        case .createPassword:
            regex = passwordRegex
        case .confirmPassword:
            regex = passwordRegex
        case .about:
            regex = textRegex
        case .password:
            regex = passwordRegex
        case .code:
            regex = codeRegex
        }
        
        let textTest = NSPredicate(format: "SELF MATCHES %@", regex)
        let valid = textTest.evaluate(with: textField.text)
        return valid
    }
    
    //MARK: UITextFieldDelegate
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        setValidState(textField: textField)
    }
    
    private func setValidState(textField: UITextField) {
        if isValid() {
            setState(state: .normal)
            errorLabel.text = ""
            self.heightConstraint?.constant = 80
        } else {
            setErrorMessage()
            setState(state: .error)
            self.heightConstraint?.constant = 90
        }
        UIView.animate(withDuration: animationTime) {
            AppDelegate.topViewController()?.view?.layoutIfNeeded()
            self.errorLabel.alpha = 1
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text?.isEmpty == true  {
            if inputType == .phone || inputType == .login {
                textField.text = "+380"
            }
        }
        self.errorLabel.alpha = 0
        setState(state: .normal)
        setState(state: .input)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
       // Try to find next responder
        if let nextField = AppDelegate.topViewController()?.view?.viewWithTag(textField.tag + 1) as? UITextField {
          nextField.becomeFirstResponder()
       } else {
          // Not found, so remove keyboard.
          textField.resignFirstResponder()
       }
       // Do not add a line break
       return false
    }
    
     func setErrorMessage() {
        var message = ""
        switch inputType {
        case .name:
            message = String.lc_emptyFieldError
        case .lastName:
            message = String.lc_emptyFieldError
        case .email:
            message = String.lc_incorrectEmail
        case .phone:
            message = String.lc_incorrectPhone
        case .createPassword:
            message = String.lc_lessThatSixChar
        case .confirmPassword:
            message = String.lc_passwordMismatch
        case .about:
            message = String.lc_emptyFieldError
        case .login:
            message = String.lc_incorrectPhone
        case .password:
            message = String.lc_lessThatSixChar
        case .code:
            message = String.lc_sixCharError
        }
        errorLabel.text = message
    }
    
    
    
    
}
