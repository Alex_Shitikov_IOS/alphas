//
//  ALAccountAccess.swift
//  Alpha
//
//  Created by alexandr shitikov on 06.10.2020.
//  Copyright © 2020 alexandr shitikov. All rights reserved.
//

import UIKit

enum ALAccountAccessType {
    case noAccess
    case noContacts
}

class ALAccountAccess: UIView {

    @IBOutlet var mainView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var iconImage: UIImageView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("ALAccountAccess", owner: self, options: nil)
        addSubview(mainView)
        mainView.frame = self.bounds
        mainView.clipsToBounds = true
        mainView.backgroundColor = .white
        titleLabel.textColor = .mainTextBlack
        descriptionLabel.textColor = .textGray
        titleLabel.text = String.lc_checkTitle
        descriptionLabel.text = String.lc_checkDescription
        iconImage.image = #imageLiteral(resourceName: "forgot_password")
    }
    
    func set(type:ALAccountAccessType) {
        switch type {
        case .noAccess:
            titleLabel.text = String.lc_checkTitle
            descriptionLabel.text = String.lc_checkDescription
            iconImage.image = #imageLiteral(resourceName: "forgot_password")
        case .noContacts:
            titleLabel.text = ""
            descriptionLabel.text = String.lc_chatNoMessgaes
            iconImage.image = #imageLiteral(resourceName: "messageIcon")
        }
    }
}
