//
//  ALAlertViewController.swift
//  Alpha
//
//  Created by alexandr shitikov on 29.09.2020.
//  Copyright © 2020 alexandr shitikov. All rights reserved.
//

import UIKit

enum ALAlertType {
    case error
    case success
}

struct AlertData {
    let title:String?
    let message:String?
    let type:ALAlertType
}

class ALAlertViewController: UIViewController {
    
    @IBOutlet weak var card: UIView!
    @IBOutlet weak var iconType: UIImageView!
    @IBOutlet weak var titleText: UILabel!
    @IBOutlet weak var messageText: UILabel!
    @IBOutlet weak var okButton: UIButton!
    var alertData:AlertData?
    var action:Selector?
    weak var target:UIViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        okButton.setGradientBackground(colors: [.orangeButton, .lightOrangeButton], cornerRadius: 0, animation: false)
        okButton.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        card.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        card.layer.cornerRadius = 12
        okButton.layer.cornerRadius = 12
        titleText.textColor = .mainTextBlack
        okButton.setTitleColor(.white, for: .normal)
        okButton.setTitle(String.lc_goodButtonTitle, for: .normal)
        messageText.textColor = .textGray
        
        switch alertData?.type {
        case .success:
            iconType.image = #imageLiteral(resourceName: "success")
        case .error:
            iconType.image = #imageLiteral(resourceName: "error")
        case .none:
            print("")
        }
        
        titleText.text = alertData?.title
        messageText.text = alertData?.message
        
    }
    
    static func show(data:AlertData, target:UIViewController, action:Selector? = nil) {
        let controller = ALStoryboard.Alert.initController(withId: "ALAlertViewController") as! ALAlertViewController
        controller.alertData = data
        controller.action = action
        controller.target = target
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalTransitionStyle = .crossDissolve
        target.present(controller, animated: true, completion: nil)
    }
    
    
    @IBAction func doneAction(_ sender: Any) {
        if let userAction = self.action {
            self.dismiss(animated: true) {
                self.target?.performSelector(onMainThread: userAction, with: nil, waitUntilDone: false)
            }
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
}
