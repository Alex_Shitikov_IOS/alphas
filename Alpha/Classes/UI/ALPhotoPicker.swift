//
//  ALPhotoPicker.swift
//  Alpha
//
//  Created by alexandr shitikov on 25.09.2020.
//  Copyright © 2020 alexandr shitikov. All rights reserved.
//

import Foundation
import UIKit

protocol ALPhotoPickerDelegate: class {
    func fileIsSelected(info: [UIImagePickerController.InfoKey : Any])
}

class CLPhotoPicker: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    weak var target: UIViewController?
    var selectedPhoto:UIImage?
    var imageView:UIImageView?
    let imagePicker = UIImagePickerController()
    weak var delegate: ALPhotoPickerDelegate?
    
    func chosePhoto() {
        self.imagePicker.delegate = self
        let title = String.lc_PhotoPicker_title
        let message = String.lc_PhotoPicker_message
        let alert = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        let photoAction = UIAlertAction(title: String.lc_PhotoPicker_galary, style: .default) { (actionPhoto) in
            self.startPhotoLib()
        }
        let cameraAction = UIAlertAction(title: String.lc_PhotoPicker_camera, style: .default) { (cameraAction) in
            self.startCamera()
        }
        
        let cancelAction = UIAlertAction(title: String.lc_PhotoPicker_cancel, style: .cancel) { (action) in
            
        }
        alert.addAction(photoAction)
        alert.addAction(cameraAction)
        alert.addAction(cancelAction)
        target?.present(alert, animated: true, completion: nil)
    }
    
    func startCamera() {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .camera
        target?.present(imagePicker, animated: true, completion: nil)
    }
    
   @objc func startPhotoLib() {
        self.imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .savedPhotosAlbum
        imagePicker.mediaTypes = ["public.image"]
        target?.present(imagePicker, animated: true, completion: nil)
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.target?.dismiss(animated: true, completion: {
                self.delegate?.fileIsSelected(info: info)
            })
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        target?.dismiss(animated: true, completion: nil)
    }
}

