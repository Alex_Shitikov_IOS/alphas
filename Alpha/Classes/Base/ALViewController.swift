//
//  ALViewController.swift
//  Alpha
//
//  Created by alexandr shitikov on 11.07.2020.
//  Copyright © 2020 alexandr shitikov. All rights reserved.
//

import UIKit

class ALViewController: UIViewController {
    
    var backgroundImageView:UIImageView?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUi()
        // Do any additional setup after loading the view.
    }
    
    func haveAccess() -> Bool {
        if UserStatus.verified.rawValue == APIManager.manager.currentUser?.status {
            return true
        } else {
            return false
        }
    }
    
    open func configureUi() {
    }
    
    func setBackgroundImage(image:UIImage?, alpha:CGFloat? = 1.0, backgroundView:UIView? = nil) {
        let constOffset:CGFloat = 0
        if self.backgroundImageView == nil {
            let newRect = CGRect(x: self.view.bounds.minX - constOffset, y: self.view.bounds.minY - constOffset, width: self.view.bounds.width + constOffset, height: self.view.bounds.height + constOffset)
            self.backgroundImageView = UIImageView(frame: newRect)
            self.backgroundImageView?.backgroundColor = UIColor.white
            self.backgroundImageView?.contentMode =  UIView.ContentMode.scaleAspectFill
            self.backgroundImageView?.clipsToBounds = true
            self.backgroundImageView?.center = self.view.center
            if backgroundView == nil {
                self.view.addSubview(self.backgroundImageView!)
                self.view.sendSubviewToBack(self.backgroundImageView!)
            } else {
                backgroundView!.addSubview(self.backgroundImageView!)
                backgroundView!.sendSubviewToBack(self.backgroundImageView!)
            }
            
            self.backgroundImageView?.image = #imageLiteral(resourceName: "bg")
            self.backgroundImageView?.alpha = alpha ?? 1.0
        }
        UIView.transition(with: self.backgroundImageView!, duration: 0.1, options: .transitionCrossDissolve, animations: {
            if let newImage = image {
                self.backgroundImageView?.image = newImage
            } else {
                self.backgroundImageView?.image = #imageLiteral(resourceName: "bg")
            }
        }, completion: nil)
    }
    
    func addBackButton() {
        let navButton = UIBarButtonItem(image: #imageLiteral(resourceName: "chevron-left"), style: .plain, target: self, action: #selector(backAction))
        navButton.tintColor = .black
        self.navigationItem.setLeftBarButton(navButton, animated: false)
    }
    
    @objc func backAction() {
        self.dismiss(animated: true, completion: nil)
    }
}
