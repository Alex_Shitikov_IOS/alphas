//
//  ALForgotPasswordCodeRouter.swift
//  Alpha
//
//  Created by alexandr shitikov on 24.09.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.

import UIKit

protocol ALForgotPasswordCodeRoutingLogic {
    func navigateToNewPassword(request:ALForgotPassword.Request?)
}

protocol ALForgotPasswordCodeDataPassing
{
    var dataStore: ALForgotPasswordCodeDataStore? { get }
}

class ALForgotPasswordCodeRouter: NSObject, ALForgotPasswordCodeRoutingLogic, ALForgotPasswordCodeDataPassing
{
    weak var viewController: ALForgotPasswordCodeViewController?
    var dataStore: ALForgotPasswordCodeDataStore?
    
    let newPasswordControllerId = "ALForgotPasswordNewPasswordViewController"
    
    // MARK: Routing
    
    func navigateToNewPassword(request:ALForgotPassword.Request?) {
        let controller = ALStoryboard.Authorization.initController(withId: newPasswordControllerId) as! ALForgotPasswordNewPasswordViewController
        controller.request = request
        viewController?.navigationController?.pushViewController(controller, animated: true)
    }    
    
}
