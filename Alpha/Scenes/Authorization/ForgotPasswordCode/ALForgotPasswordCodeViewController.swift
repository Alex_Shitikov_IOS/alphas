//
//  ALForgotPasswordCodeViewController.swift
//  Alpha
//
//  Created by alexandr shitikov on 24.09.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.


import UIKit

protocol ALForgotPasswordCodeDisplayLogic: class {
    func handleSuccessResendCode()
    func handleSuccess()
    func handleError(message:String)
}

class ALForgotPasswordCodeViewController: ALViewController, ALForgotPasswordCodeDisplayLogic {
    var interactor: ALForgotPasswordCodeBusinessLogic?
    var router: (NSObjectProtocol & ALForgotPasswordCodeRoutingLogic & ALForgotPasswordCodeDataPassing)?
    var request:ALForgotPassword.Request?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var codeInputView: ALTextInput!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var sendCodeDescription: UILabel!
    @IBOutlet weak var sendCodeButton: UIButton!
    
    // MARK: Object lifecycle
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: Setup
    
    private func setup() {
        let viewController = self
        let interactor = ALForgotPasswordCodeInteractor()
        let presenter = ALForgotPasswordCodePresenter()
        let router = ALForgotPasswordCodeRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    // MARK: View lifecycle
    
    override func configureUi() {
        super.configureUi()
        codeInputView.setViewType(type: .code, tag: 1)
        titleLabel.textColor = .black
        descriptionLabel.textColor = .textGray
        sendCodeDescription.textColor = .textGray
        
        doneButton.rounded(radius: 18)
        doneButton.setTitleColor(.white, for: .normal)
        doneButton.setGradientBackground(colors: [.orangeButton, .lightOrangeButton], cornerRadius: 18, animation: true)
        doneButton.setTitle(String.lc_forgotConfirm, for: .normal)
        titleLabel.text = String.lc_forgotConfirmCode
        descriptionLabel.text = String.lc_forgotMessage + " \(request?.phone ?? "") " + String.lc_forgotMessagePartTwo
        sendCodeDescription.text = String.lc_forgotNoCodeDescription
        sendCodeButton.setTitle(String.lc_forgotSendCodeButton, for: .normal)
        sendCodeButton.setTitleColor(.orangeButton, for: .normal)
        title = String.lc_forgotVerifyPhoneTitle
    }
    
    
    @IBAction func doneAction(_ sender: Any) {
        if codeInputView.isValid() {
            view.startLoader()
            let requestCode = ALForgotPassword.Request(phone: self.request?.phone,
                                                       code: codeInputView.textField.text,
                                                       newPassword: nil)
            self.request = requestCode
            interactor?.sendCodeConfirm(request: requestCode)
        }
    }
    
    @IBAction func sendCodeAction(_ sender: Any) {
        view.startLoader()
        interactor?.getCode(request: self.request)
    }
}
