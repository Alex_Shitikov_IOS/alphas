//
//  ALForgotPasswordCodeInteractor.swift
//  Alpha
//
//  Created by alexandr shitikov on 24.09.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.
//
//

import UIKit

protocol ALForgotPasswordCodeBusinessLogic {
    func getCode(request:ALForgotPassword.Request?)
    func sendCodeConfirm(request:ALForgotPassword.Request)
}

protocol ALForgotPasswordCodeDataStore {
    //var name: String { get set }
}

class ALForgotPasswordCodeInteractor: ALForgotPasswordCodeBusinessLogic, ALForgotPasswordCodeDataStore {
    var presenter: ALForgotPasswordCodePresentationLogic?
    var worker: ALForgotPasswordCodeWorker?
    
    // MARK: ALForgotPasswordCodeBusinessLogic
    
    func sendCodeConfirm(request:ALForgotPassword.Request) {
        let url = PCURLs.Password.validate.rawValue
        
        APIManager.manager.performRequestWith(method: .POST, url: url, params: request.dictionary, showLoader: false, completion: { (data, response, error) in
            if response?.isSuccess() == true {
                self.presenter?.handleSuccess()
            } else {
                self.presenter?.handleError(error: error)
            }
        }, resourceType: .noCredentials)
    }
    
    func getCode(request:ALForgotPassword.Request?) {
        let url = PCURLs.Password.forgot.rawValue
        APIManager.manager.performRequestWith(method: .POST, url: url, params: request.dictionary, showLoader: false, completion: { (data, response, error) in
            if response?.isSuccess() == true {
                self.presenter?.handleSuccessResendCode()
            } else {
                self.presenter?.handleError(error: error)
            }
        }, resourceType: .noCredentials)
    }
    
}
