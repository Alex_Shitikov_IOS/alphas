//
//  ALForgotPasswordCodePresenter.swift
//  Alpha
//
//  Created by alexandr shitikov on 24.09.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.


import UIKit

protocol ALForgotPasswordCodePresentationLogic {
    func handleSuccess()
    func handleError(error:ALResponseError?)
    func handleSuccessResendCode()
}

class ALForgotPasswordCodePresenter: ALForgotPasswordCodePresentationLogic {
    weak var viewController: ALForgotPasswordCodeDisplayLogic?
    
    // MARK: ALForgotPasswordCodePresentationLogic
    
    func handleSuccess() {
        viewController?.handleSuccess()
    }
    
    func handleError(error:ALResponseError?) {
        viewController?.handleError(message: error?.message ?? String.lc_serverErrorMessage)
    }
    
    func handleSuccessResendCode() {
        viewController?.handleSuccessResendCode()
    }
    
}
