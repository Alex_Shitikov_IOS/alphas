//
//  ALForgotPasswordCodeControllerDelegate.swift
//  Alpha
//
//  Created by alexandr shitikov on 24.09.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.

import UIKit

extension ALForgotPasswordCodeViewController {
    
    func handleSuccess() {
        view.stopLoader()
        router?.navigateToNewPassword(request: request)
    }
    
    func handleError(message:String) {
        view.stopLoader()
        let data = AlertData(title: message, message: nil, type: .error)
        ALAlertViewController.show(data: data, target: self)
    }
    
    func handleSuccessResendCode() {
        view.stopLoader()
        let data = AlertData(title: String.lc_forgotResendsuccessMessage, message: nil, type: .success)
        ALAlertViewController.show(data: data, target: self)
    }
    
}
