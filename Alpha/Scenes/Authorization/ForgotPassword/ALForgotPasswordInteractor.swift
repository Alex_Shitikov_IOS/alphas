//
//  ALForgotPasswordInteractor.swift
//  Alpha
//
//  Created by alexandr shitikov on 21.09.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.
//
//

import UIKit

protocol ALForgotPasswordBusinessLogic {
    func getCode(request:ALForgotPassword.Request)
}

protocol ALForgotPasswordDataStore {
    //var name: String { get set }
}

class ALForgotPasswordInteractor: ALForgotPasswordBusinessLogic, ALForgotPasswordDataStore {
    var presenter: ALForgotPasswordPresentationLogic?
    var worker: ALForgotPasswordWorker?
    
    // MARK: ALForgotPasswordBusinessLogic
    
    func getCode(request:ALForgotPassword.Request) {
        let url = PCURLs.Password.forgot.rawValue
        APIManager.manager.performRequestWith(method: .POST, url: url, params: request.dictionary, showLoader: false, completion: { (data, response, error) in
            if response?.isSuccess() == true {
                self.presenter?.handleSuccess()
            } else {
                self.presenter?.handleError(error: error)
            }
        }, resourceType: .noCredentials)
    }
}
