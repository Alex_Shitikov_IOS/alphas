//
//  ALForgotPasswordRouter.swift
//  Alpha
//
//  Created by alexandr shitikov on 21.09.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.

import UIKit

protocol ALForgotPasswordRoutingLogic {
    func navigateToCode(request:ALForgotPassword.Request?)
}

protocol ALForgotPasswordDataPassing {
    var dataStore: ALForgotPasswordDataStore? { get }
}

class ALForgotPasswordRouter: NSObject, ALForgotPasswordRoutingLogic, ALForgotPasswordDataPassing {
    weak var viewController: ALForgotPasswordViewController?
    var dataStore: ALForgotPasswordDataStore?
    let codeId = "ALForgotPasswordCodeViewController"
    
    //MARK: ALForgotPasswordRoutingLogic
    
    func navigateToCode(request:ALForgotPassword.Request?) {
        let controller = ALStoryboard.Authorization.initController(withId: codeId) as! ALForgotPasswordCodeViewController
        controller.request = request
        viewController?.navigationController?.pushViewController(controller, animated: true)
    }

}
