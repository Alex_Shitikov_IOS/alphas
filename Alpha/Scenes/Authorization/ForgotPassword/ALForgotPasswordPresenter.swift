//
//  ALForgotPasswordPresenter.swift
//  Alpha
//
//  Created by alexandr shitikov on 21.09.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.


import UIKit

protocol ALForgotPasswordPresentationLogic {
    func handleSuccess()
    func handleError(error:ALResponseError?)
}

class ALForgotPasswordPresenter: ALForgotPasswordPresentationLogic {
  weak var viewController: ALForgotPasswordDisplayLogic?
  
  // MARK: ALForgotPasswordPresentationLogic
    
    func handleSuccess() {
        viewController?.handleSuccess()
    }
    
    func handleError(error:ALResponseError?) {
        viewController?.handleError(message: error?.message ?? String.lc_serverErrorMessage)
    }
}
