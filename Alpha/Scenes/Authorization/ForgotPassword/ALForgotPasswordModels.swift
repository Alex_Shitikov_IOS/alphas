//
//  ALForgotPasswordModels.swift
//  Alpha
//
//  Created by alexandr shitikov on 21.09.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.


import UIKit

enum ALForgotPassword {
    
    // MARK: - Request
    struct Request: Codable {
        let phone: String?
        let code: String?
        let newPassword: String?

        enum CodingKeys: String, CodingKey {
            case phone = "phone"
            case code = "code"
            case newPassword = "newPassword"
        }
    }
  
}
