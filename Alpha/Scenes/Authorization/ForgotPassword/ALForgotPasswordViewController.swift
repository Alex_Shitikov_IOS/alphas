//
//  ALForgotPasswordViewController.swift
//  Alpha
//
//  Created by alexandr shitikov on 21.09.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.


import UIKit

protocol ALForgotPasswordDisplayLogic: class {
    func handleSuccess()
    func handleError(message: String)
}

class ALForgotPasswordViewController: ALViewController, ALForgotPasswordDisplayLogic {
    var interactor: ALForgotPasswordBusinessLogic?
    var router: (NSObjectProtocol & ALForgotPasswordRoutingLogic & ALForgotPasswordDataPassing)?
    private let cornerRadius: Double = 18
    
    @IBOutlet weak var forgotPasswordLabel: UILabel!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var phoneInput: ALTextInput!
    @IBOutlet weak var sendButton: UIButton!
    
    // MARK: Object lifecycle
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: Setup
    
    private func setup() {
        let viewController = self
        let interactor = ALForgotPasswordInteractor()
        let presenter = ALForgotPasswordPresenter()
        let router = ALForgotPasswordRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    // MARK: View lifecycle
    
    override func configureUi() {
        super.configureUi()
        phoneInput.setViewType(type: .phone, tag: 1)
        sendButton.rounded(radius: cornerRadius)
        sendButton.setTitleColor(.white, for: .normal)
        sendButton.setGradientBackground(colors: [.orangeButton, .lightOrangeButton], cornerRadius: CGFloat(cornerRadius), animation: true)
        sendButton.setTitle(String.lc_forgotConfirm, for: .normal)
        addBackButton()
        title = String.lc_forgotMainTitle
    }
    
    @IBAction func sendAction(_ sender: Any) {
        view.endEditing(true)
        if phoneInput.isValid() {
            view.startLoader()
            let request = ALForgotPassword.Request(phone: phoneInput.textField.text!,
                                                   code: nil,
                                                   newPassword: nil)
            interactor?.getCode(request: request)
        }
    }
}
