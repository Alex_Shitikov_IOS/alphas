//
//  ALForgotPasswordControllerDelegate.swift
//  Alpha
//
//  Created by alexandr shitikov on 21.09.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.

import UIKit

extension ALForgotPasswordViewController {
    
    //MARK: ALForgotPasswordDisplayLogic
    
    func handleSuccess() {
        view.stopLoader()
        let request = ALForgotPassword.Request(phone: phoneInput.textField.text!,
                                               code: nil,
                                               newPassword: nil)
        router?.navigateToCode(request: request)
    }
    
    func handleError(message:String) {
        view.stopLoader()
        let data = AlertData(title: message, message: nil, type: .error)
        ALAlertViewController.show(data: data, target: self)
    }
}
