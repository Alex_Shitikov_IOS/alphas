//
//  ALForgotPasswordNewPasswordControllerDelegate.swift
//  Alpha
//
//  Created by alexandr shitikov on 25.09.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.

import UIKit

extension ALForgotPasswordNewPasswordViewController {
    
    func handleSuccess() {
        let data = AlertData(title: String.lc_forgotPasswordChangedsuccessMessage,
                             message: nil,
                             type: .success)
        ALAlertViewController.show(data: data, target: self, action: #selector(doneAction))
    }
    
    func handleError(message: String) {
        let data = AlertData(title: message, message: nil, type: .error)
        ALAlertViewController.show(data: data, target: self)
    }
    
    @objc func doneAction() {
        self.dismiss(animated: true, completion: nil)
    }
}
