//
//  ALForgotPasswordNewPasswordViewController.swift
//  Alpha
//
//  Created by alexandr shitikov on 25.09.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.


import UIKit

protocol ALForgotPasswordNewPasswordDisplayLogic: class {
    func handleSuccess()
    func handleError(message:String)
}

class ALForgotPasswordNewPasswordViewController: ALViewController, ALForgotPasswordNewPasswordDisplayLogic
{
    var interactor: ALForgotPasswordNewPasswordBusinessLogic?
    var router: (NSObjectProtocol & ALForgotPasswordNewPasswordRoutingLogic & ALForgotPasswordNewPasswordDataPassing)?
    
    var request:ALForgotPassword.Request?

    
    @IBOutlet weak var newPasswordView: ALTextInput!
    
    @IBOutlet weak var confirmPasswordView: ALTextInput!
    
    @IBOutlet weak var confirmButton: UIButton!
    // MARK: Object lifecycle
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)
    {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: Setup
    
    private func setup()
    {
        let viewController = self
        let interactor = ALForgotPasswordNewPasswordInteractor()
        let presenter = ALForgotPasswordNewPasswordPresenter()
        let router = ALForgotPasswordNewPasswordRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    // MARK: View lifecycle
    
    override func configureUi() {
        super.configureUi()
        title = String.lc_forgotNewPasswordTitle
        confirmButton.rounded(radius: 18)
        confirmButton.setTitleColor(.white, for: .normal)
        confirmButton.setGradientBackground(colors: [.orangeButton, .lightOrangeButton], cornerRadius: 18, animation: true)
        confirmButton.setTitle(String.lc_forgotConfirm, for: .normal)
        newPasswordView.setViewType(type: .createPassword, tag: 1)
        confirmPasswordView.setViewType(type: .confirmPassword, tag: 2)
    }
    
    
    @IBAction func confirmAction(_ sender: Any) {
        
        if newPasswordView.isValid() && confirmPasswordView.isValid() {
            if newPasswordView.textField.text != confirmPasswordView.textField.text {
                confirmPasswordView.setErrorMessage()
                confirmPasswordView.setState(state: .error)
            } else {
                let passwordRequest = ALForgotPassword.Request(phone: request?.phone,
                                                               code: request?.code,
                                                               newPassword: confirmPasswordView.textField.text)
                interactor?.createPassword(request: passwordRequest)
            }
        }
        
    }
    
}
