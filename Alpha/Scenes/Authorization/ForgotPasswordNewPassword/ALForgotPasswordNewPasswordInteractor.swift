//
//  ALForgotPasswordNewPasswordInteractor.swift
//  Alpha
//
//  Created by alexandr shitikov on 25.09.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.
//
//

import UIKit

protocol ALForgotPasswordNewPasswordBusinessLogic {
    func createPassword(request:ALForgotPassword.Request?)
}

protocol ALForgotPasswordNewPasswordDataStore {
    //var name: String { get set }
}

class ALForgotPasswordNewPasswordInteractor: ALForgotPasswordNewPasswordBusinessLogic, ALForgotPasswordNewPasswordDataStore {
    var presenter: ALForgotPasswordNewPasswordPresentationLogic?
    var worker: ALForgotPasswordNewPasswordWorker?
    
    // MARK: ALForgotPasswordNewPasswordBusinessLogic
    
    func createPassword(request: ALForgotPassword.Request?) {
        let url = PCURLs.Password.reset.rawValue
        APIManager.manager.performRequestWith(method: .POST, url: url, params: request?.dictionary, showLoader: false, completion: { (data, response, error) in
            if response?.isSuccess() == true {
                self.presenter?.handleSuccess()
            } else {
                self.presenter?.handleError(error: error)
            }
            
        }, resourceType: .noCredentials)
    }
    
}
