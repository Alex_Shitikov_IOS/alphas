//
//  ALForgotPasswordNewPasswordPresenter.swift
//  Alpha
//
//  Created by alexandr shitikov on 25.09.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.


import UIKit

protocol ALForgotPasswordNewPasswordPresentationLogic {
    func handleSuccess()
    func handleError(error:ALResponseError?)
}

class ALForgotPasswordNewPasswordPresenter: ALForgotPasswordNewPasswordPresentationLogic {
    weak var viewController: ALForgotPasswordNewPasswordDisplayLogic?
    
    // MARK: ALForgotPasswordNewPasswordPresentationLogic
    
    func handleSuccess() {
        viewController?.handleSuccess()
    }
    
    func handleError(error:ALResponseError?) {
        DispatchQueue.main.async {
            self.viewController?.handleError(message: error?.message ?? String.lc_serverErrorMessage)
        }
    }
    
}
