//
//  ALForgotPasswordNewPasswordRouter.swift
//  Alpha
//
//  Created by alexandr shitikov on 25.09.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.

import UIKit

@objc protocol ALForgotPasswordNewPasswordRoutingLogic {
  
}

protocol ALForgotPasswordNewPasswordDataPassing
{
  var dataStore: ALForgotPasswordNewPasswordDataStore? { get }
}

class ALForgotPasswordNewPasswordRouter: NSObject, ALForgotPasswordNewPasswordRoutingLogic, ALForgotPasswordNewPasswordDataPassing
{
  weak var viewController: ALForgotPasswordNewPasswordViewController?
  var dataStore: ALForgotPasswordNewPasswordDataStore?
  
  // MARK: Routing
  
}
