//
//  ALPhotoGalleryPhotoPresenter.swift
//  Alpha
//
//  Created by alexandr shitikov on 05.10.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.


import UIKit

protocol ALPhotoGalleryPhotoPresentationLogic {
    func handleSuccess(image:UIImage)
    func handleError(error:ALResponseError?)
}

class ALPhotoGalleryPhotoPresenter: ALPhotoGalleryPhotoPresentationLogic {
    weak var viewController: ALPhotoGalleryPhotoDisplayLogic?
    
    // MARK: ALPhotoGalleryPhotoPresentationLogic
    
    func handleSuccess(image:UIImage) {
        let item = ALPhotoGalleryPhoto.ViewModel(image: image)
        viewController?.handleSuccess(photo: item)
    }
    
    func handleError(error:ALResponseError?) {
        viewController?.handleError(message: error?.message ?? String.lc_serverErrorMessage)
    }
}
