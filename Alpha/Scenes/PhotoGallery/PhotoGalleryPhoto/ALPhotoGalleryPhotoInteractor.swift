//
//  ALPhotoGalleryPhotoInteractor.swift
//  Alpha
//
//  Created by alexandr shitikov on 05.10.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.
//
//

import UIKit

protocol ALPhotoGalleryPhotoBusinessLogic {
    func loadPhoto(url:String)
}

protocol ALPhotoGalleryPhotoDataStore {
}

class ALPhotoGalleryPhotoInteractor: ALPhotoGalleryPhotoBusinessLogic, ALPhotoGalleryPhotoDataStore {
    var presenter: ALPhotoGalleryPhotoPresentationLogic?
    
    // MARK: ALPhotoGalleryPhotoBusinessLogic
    
    func loadPhoto(url:String) {
        APIManager.manager.getPhoto(url: url) { (image) in
            if let photo = image {
                self.presenter?.handleSuccess(image: photo)
            } else {
                self.presenter?.handleError(error: nil)
            }
        }
    }
    
}
