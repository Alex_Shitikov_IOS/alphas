//
//  ALPhotoGalleryPhotoRouter.swift
//  Alpha
//
//  Created by alexandr shitikov on 05.10.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.

import UIKit

@objc protocol ALPhotoGalleryPhotoRoutingLogic {
  
}

protocol ALPhotoGalleryPhotoDataPassing
{
  var dataStore: ALPhotoGalleryPhotoDataStore? { get }
}

class ALPhotoGalleryPhotoRouter: NSObject, ALPhotoGalleryPhotoRoutingLogic, ALPhotoGalleryPhotoDataPassing
{
  weak var viewController: ALPhotoGalleryPhotoViewController?
  var dataStore: ALPhotoGalleryPhotoDataStore?
  
  // MARK: Routing
  
}
