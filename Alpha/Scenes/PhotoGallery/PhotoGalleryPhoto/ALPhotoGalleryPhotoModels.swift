//
//  ALPhotoGalleryPhotoModels.swift
//  Alpha
//
//  Created by alexandr shitikov on 05.10.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.


import UIKit

enum ALPhotoGalleryPhoto {
    
    // MARK: Use cases
    
    struct ViewModel {
        let image:UIImage
    }
}
