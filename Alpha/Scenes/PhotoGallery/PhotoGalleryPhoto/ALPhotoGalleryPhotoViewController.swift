//
//  ALPhotoGalleryPhotoViewController.swift
//  Alpha
//
//  Created by alexandr shitikov on 05.10.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.


import UIKit

protocol ALPhotoGalleryPhotoDisplayLogic: AnyObject {
    func handleSuccess(photo: ALPhotoGalleryPhoto.ViewModel)
    func handleError(message: String)
}

class ALPhotoGalleryPhotoViewController: ALViewController, ALPhotoGalleryPhotoDisplayLogic {
    
    var interactor: ALPhotoGalleryPhotoBusinessLogic?
    var router: (NSObjectProtocol & ALPhotoGalleryPhotoRoutingLogic & ALPhotoGalleryPhotoDataPassing)?
    var requestImage: UIImage?
    var requestUrl: String = ""
    
    // MARK: Object lifecycle
    @IBOutlet weak var photoImage: UIImageView!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: Setup
    
    private func setup() {
        let viewController = self
        let interactor = ALPhotoGalleryPhotoInteractor()
        let presenter = ALPhotoGalleryPhotoPresenter()
        let router = ALPhotoGalleryPhotoRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    // MARK: View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        photoImage.image = requestImage
        photoImage.enableZoom()
        view.backgroundColor = .black
        backButton.tintColor = .white
        shareButton.tintColor = .white
        interactor?.loadPhoto(url: requestUrl)
        
    }
    
    @IBAction func exitAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func shareAction(_ sender: Any) {
        shareImage(image: photoImage.image)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.shouldRotate = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.shouldRotate = true
    }
    
    override func transition(from fromViewController: UIViewController, to toViewController: UIViewController, duration: TimeInterval, options: UIView.AnimationOptions = [], animations: (() -> Void)?, completion: ((Bool) -> Void)? = nil) {
    }
    
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        coordinator.animate(alongsideTransition: { context in
            if UIApplication.shared.statusBarOrientation.isLandscape {
                // activate landscape changes
                self.backButton.isHidden = true
                UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
                }
            } else if UIApplication.shared.statusBarOrientation.isPortrait {
                // activate portrait changes
                self.backButton.isHidden = false
                UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
                }
            }
        })
    }
    
    
}
