//
//  ALPhotoGalleryPhotoControllerDelegate.swift
//  Alpha
//
//  Created by alexandr shitikov on 05.10.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.

import UIKit

extension ALPhotoGalleryPhotoViewController {
    
    func shareImage(image:UIImage?) {
        let imageToShare = [image!]
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    // MARK: ALPhotoGalleryPhotoDisplayLogic
    
    func handleSuccess(photo: ALPhotoGalleryPhoto.ViewModel) {
        self.photoImage.image = photo.image
    }
    
    func handleError(message: String) {
        let data = AlertData(title: message, message: nil, type: .error)
        ALAlertViewController.show(data: data, target: self)
    }
    
}
