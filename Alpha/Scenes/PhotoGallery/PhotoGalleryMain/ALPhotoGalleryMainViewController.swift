//
//  ALPhotoGalleryMainViewController.swift
//  Alpha
//
//  Created by alexandr shitikov on 05.10.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.


import UIKit

protocol ALPhotoGalleryMainDisplayLogic: AnyObject {
    func handleSuccess(photos: [ALPhotoGalleryMain.ViewModel], event: ALPhotoGalleryMain.Event?)
    func handleError(message:String)
}

class ALPhotoGalleryMainViewController: ALViewController, ALPhotoGalleryMainDisplayLogic {
    var interactor: ALPhotoGalleryMainBusinessLogic?
    var router: (NSObjectProtocol & ALPhotoGalleryMainRoutingLogic & ALPhotoGalleryMainDataPassing)?
    
    let cellId = "ALPhotoGallaryCell"
    var dataArray = [ALPhotoGalleryMain.ViewModel]()
    var event: ALProfile.EventsResponseElement?
    private let cornerRadius: CGFloat = 16
    
    @IBOutlet weak var navigationHeaderView: UIView!
    @IBOutlet weak var mainContentView: UIView!
    @IBOutlet weak var topOffset: NSLayoutConstraint!
    @IBOutlet weak var headerImage: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var mainTitle: UILabel!
    
    // MARK: Object lifecycle
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: Setup
    
    private func setup() {
        let viewController = self
        let interactor = ALPhotoGalleryMainInteractor()
        let presenter = ALPhotoGalleryMainPresenter()
        let router = ALPhotoGalleryMainRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    // MARK: View lifecycle
    
    override func configureUi() {
        super.configureUi()
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: cellId, bundle: nil), forCellWithReuseIdentifier: cellId)
        dateLabel.textColor = .textGray
        titleLabel.textColor = .mainTextBlack
        descriptionLabel.textColor = .mainTextBlack
        separatorView.backgroundColor = .textGray
        backButton.tintColor = .white
        mainTitle.textColor = .white
        mainTitle.text = String.lc_photoGalleryTitle
        navigationHeaderView.backgroundColor = .black05
        mainContentView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        mainContentView.layer.cornerRadius = cornerRadius
        view.startLoader()
        interactor?.getPhoto(id: event?.id ?? "")
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
