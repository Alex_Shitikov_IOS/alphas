//
//  ALPhotoGalleryMainRouter.swift
//  Alpha
//
//  Created by alexandr shitikov on 05.10.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.

import UIKit

@objc protocol ALPhotoGalleryMainRoutingLogic {
    func showPhoto(image: UIImage?, url: String?)
}

protocol ALPhotoGalleryMainDataPassing {
    var dataStore: ALPhotoGalleryMainDataStore? { get }
}

class ALPhotoGalleryMainRouter: NSObject, ALPhotoGalleryMainRoutingLogic, ALPhotoGalleryMainDataPassing {
    weak var viewController: ALPhotoGalleryMainViewController?
    var dataStore: ALPhotoGalleryMainDataStore?
    let photoId = "ALPhotoGalleryPhotoViewController"
    
    // MARK: Routing
    
    func showPhoto(image: UIImage?, url: String?) {
        let controller = ALStoryboard.PhotoGallery.initController(withId: photoId) as! ALPhotoGalleryPhotoViewController
        controller.requestImage = image
        controller.requestUrl = url ?? ""
        controller.modalPresentationStyle = .fullScreen
        viewController?.present(controller, animated: true, completion: nil)
    }
    
}
