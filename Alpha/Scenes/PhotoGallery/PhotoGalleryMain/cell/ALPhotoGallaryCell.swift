//
//  ALPhotoGallaryCell.swift
//  Alpha
//
//  Created by alexandr shitikov on 05.10.2020.
//  Copyright © 2020 alexandr shitikov. All rights reserved.
//

import UIKit

class ALPhotoGallaryCell: UICollectionViewCell {

    @IBOutlet weak var photo: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        photo.rounded(radius: 8)
        photo.clipsToBounds = true
        rounded(radius: 8)
    }
    
    func confihure(photoUrl:String?) {
        startLoader()
        self.photo.image = #imageLiteral(resourceName: "photo")
        if let urlString = photoUrl {
//            let url = URL(string: urlString)
//            var components = URLComponents(url: url!, resolvingAgainstBaseURL: false)
//            let componentsTemp = components
//            let pathArray = componentsTemp?.path.components(separatedBy: "/")
//            var newPath = ""
//            pathArray?.forEach({ (element) in
//                if element == pathArray?.last {
//                    newPath = newPath + "/min-\(element)"
//                } else if element != pathArray?.first {
//                    newPath = newPath + "/\(element)"
//                }
//            })
//            components?.path = newPath
//            let newUrl = components?.string
            APIManager.manager.getPhoto(url: urlString) { (image) in
                self.stopLoader()
                if let photo = image {
                    self.photo.image = photo
                } else {
                    self.photo.image = #imageLiteral(resourceName: "photo")
                }
            }
        }
        
    }

}
