//
//  ALPhotoGalleryMainPresenter.swift
//  Alpha
//
//  Created by alexandr shitikov on 05.10.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.


import UIKit

protocol ALPhotoGalleryMainPresentationLogic {
    func handleSuccess(event: ALPhotoGalleryMain.ALGalleryResponseElement)
    func handleError(error: ALResponseError?)
}

class ALPhotoGalleryMainPresenter: ALPhotoGalleryMainPresentationLogic {
  weak var viewController: ALPhotoGalleryMainDisplayLogic?
  
  // MARK: ALPhotoGalleryMainPresentationLogic
    
    func handleSuccess(event: ALPhotoGalleryMain.ALGalleryResponseElement) {
        var items = [ALPhotoGalleryMain.ViewModel]()
        event.gallery?.photos?.forEach({ (url) in
            let item = ALPhotoGalleryMain.ViewModel(url: url)
            items.append(item)
        })
        viewController?.handleSuccess(photos: items, event: event.event)
    }
    
    func handleError(error:ALResponseError?) {
        viewController?.handleError(message: error?.message ?? String.lc_serverErrorMessage)
    }
  
}
