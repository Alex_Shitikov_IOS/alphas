//
//  ALPhotoGalleryMainInteractor.swift
//  Alpha
//
//  Created by alexandr shitikov on 05.10.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.
//
//

import UIKit

protocol ALPhotoGalleryMainBusinessLogic {
    func getPhoto(id: String)
}

protocol ALPhotoGalleryMainDataStore {
  //var name: String { get set }
}

class ALPhotoGalleryMainInteractor: ALPhotoGalleryMainBusinessLogic, ALPhotoGalleryMainDataStore {
  var presenter: ALPhotoGalleryMainPresentationLogic?
  var worker: ALPhotoGalleryMainWorker?
  
  // MARK: ALPhotoGalleryMainBusinessLogic
    
    func getPhoto(id: String) {
        let url = PCURLs.Gallary.eventPhotos.rawValue
        APIManager.manager.performRequestWith(method: .GET,
                                              url: url,
                                              completion: { (data, response, error) in
            let events = data?.parseCodableObject(structType: ALPhotoGalleryMain.ALGalleryResponse.self)
            if let event =  events?.first(where: {$0.event?.id == id})  {
                self.presenter?.handleSuccess(event: event)
            } else {
                self.presenter?.handleError(error: error)
            }
        }, resourceType: .noCredentials)
    }
    
}


