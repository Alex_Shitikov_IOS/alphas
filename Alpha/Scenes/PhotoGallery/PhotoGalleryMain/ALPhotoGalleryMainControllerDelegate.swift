//
//  ALPhotoGalleryMainControllerDelegate.swift
//  Alpha
//
//  Created by alexandr shitikov on 05.10.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.

import UIKit

extension ALPhotoGalleryMainViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    //MARK: UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ALPhotoGallaryCell
        let item = dataArray[indexPath.item]
        cell.confihure(photoUrl: item.url)
        return cell
    }
    
    //MARK: UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! ALPhotoGallaryCell
        let image = cell.photo.image
        let item = dataArray[indexPath.item]
        router?.showPhoto(image: image, url: item.url)
    }
    
    //MARK: UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width/2 - 4
        let size = CGSize(width: width, height: width)
        return size
    }
    
    //MARK: ALPhotoGalleryMainDisplayLogic
    
    func handleSuccess(photos:[ALPhotoGalleryMain.ViewModel], event: ALPhotoGalleryMain.Event?) {
        view.stopLoader()
        dataArray.removeAll()
        dataArray = photos
        collectionView.reloadData()
        titleLabel.text = event?.name
    }
    
    func handleError(message:String) {
        view.stopLoader()
        let data = AlertData(title: message, message: nil, type: .error)
        ALAlertViewController.show(data: data, target: self)
    }
    
}
