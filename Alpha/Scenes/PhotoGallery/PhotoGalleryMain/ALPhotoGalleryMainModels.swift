//
//  ALPhotoGalleryMainModels.swift
//  Alpha
//
//  Created by alexandr shitikov on 05.10.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.


import UIKit

enum ALPhotoGalleryMain {
    
    typealias ALGalleryResponse = [ALGalleryResponseElement]
    
    // MARK: - ALGalleryResponseElement
    struct ALGalleryResponseElement: Codable {
        let event: Event?
        let gallery: Gallery?
        
        enum CodingKeys: String, CodingKey {
            case event = "event"
            case gallery = "gallery"
        }
    }
    
    // MARK: - Event
    struct Event: Codable {
        let password: String?
        let v: Int?
        let id: String?
        let active: Bool?
        let name: String?
        
        enum CodingKeys: String, CodingKey {
            case password = "password"
            case v = "__v"
            case id = "_id"
            case active = "active"
            case name = "name"
        }
    }
    
    // MARK: - Gallery
    struct Gallery: Codable {
        let photos: [String]?
        let v: Int?
        let id: String?
        let eventId: String?
        
        enum CodingKeys: String, CodingKey {
            case photos = "photos"
            case v = "__v"
            case id = "_id"
            case eventId = "eventId"
        }
    }
    
    struct ViewModel {
        let url: String
    }
    
}
