//
//  ALFavoriteChatsView.swift
//  Alpha
//
//  Created by alexandr shitikov on 07.10.2020.
//  Copyright © 2020 alexandr shitikov. All rights reserved.
//

import UIKit

protocol ALFavoriteChatsViewDelegate: class {
    func chatDidSelect(chat:ChatChannel)
}

class ALFavoriteChatsView: UIView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    let cellId = "ALFavoriteChatsViewCell"
    var dataArray = [ChatChannel]()
    weak var delegate:ALFavoriteChatsViewDelegate?
    private let cellSize = CGSize(width: 80, height: 100)
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("ALFavoriteChatsView", owner: self, options: nil)
        addSubview(mainView)
        titleLabel.text = String.lc_favoriteContactsTitle
        mainView.frame = self.bounds
        mainView.clipsToBounds = true
        mainView.backgroundColor = .clear
        titleLabel.textColor = .mainTextBlack
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.clipsToBounds = false
        collectionView.register(UINib(nibName: cellId, bundle: nil), forCellWithReuseIdentifier: cellId)
        collectionView.backgroundColor = .clear
    }
    
    //MARK: Set data to view
    func configure(contacts: [ChatChannel]) {
        dataArray.removeAll()
        dataArray = contacts
        collectionView.reloadData()
    }
    
    //MARK: UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ALFavoriteChatsViewCell
        let item = dataArray[indexPath.item]
        cell.configure(item: item)
        return cell
    }
    
    //MARK: UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = cellSize
        return size
    }
    
    //MARK: UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = dataArray[indexPath.item]
        delegate?.chatDidSelect(chat: item)
    }
    
}
