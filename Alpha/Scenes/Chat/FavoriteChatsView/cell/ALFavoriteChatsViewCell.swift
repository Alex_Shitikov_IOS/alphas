//
//  ALFavoriteChatsViewCell.swift
//  Alpha
//
//  Created by alexandr shitikov on 07.10.2020.
//  Copyright © 2020 alexandr shitikov. All rights reserved.
//

import UIKit

class ALFavoriteChatsViewCell: UICollectionViewCell {
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    private let cornerRadius: Double = 8
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cardView.rounded(radius: cornerRadius)
        cardView.clipsToBounds = true
        nameLabel.textColor = .mainTextBlack
        avatarImage.rounded(radius: cornerRadius)
    }
    
    func configure(item:ChatChannel) {
        nameLabel.text = item.title
        if item.channelType == .support && APIManager.manager.currentUser?.supportManager != true {
            avatarImage.image = #imageLiteral(resourceName: "supportIcon")
        } else {
            self.avatarImage.image = #imageLiteral(resourceName: "photo")
            TempDataManager.shared.getPhoto(url: item.photoUrl) { (image) in
                if let photo = image {
                    self.avatarImage.image = photo
                }
            }
        }
    }

}
