//
//  ALChatContactsInteractor.swift
//  Alpha
//
//  Created by alexandr shitikov on 03.10.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.
//
//

import UIKit
import TwilioChatClient

protocol ALChatContactsBusinessLogic {
    func deleteChannel(channel:TCHChannel)
    func getFavoriteChannels(chanels:[ChatChannel])
    func getChannels()
}

protocol ALChatContactsDataStore {
    //var name: String { get set }
}

class ALChatContactsInteractor: ALChatContactsBusinessLogic, ALChatContactsDataStore {
    
    
    var presenter: ALChatContactsPresentationLogic?
    var worker: ALChatContactsWorker?
    
    let workGroup = DispatchGroup()
    var checkSuopport = false
    var supportChannel:ChatChannel?
    let currentUser = APIManager.manager.currentUser
    
    
    // MARK: ALChatContactsBusinessLogic
    
    func deleteChannel(channel:TCHChannel) {
        channel.destroy { result in
            if result.isSuccessful() {
                print("Channel destroyed.")
            } else {
                print("Channel NOT destroyed.")
                channel.leave { (result) in
                    print("You leave this channel")
                }
            }
        }
    }
    
    func getFavoriteChannels(chanels:[ChatChannel]) {
        
    }
    
    func getChannels() {
        if checkSuopport == false {
            checkSuopport = true
            contactWithSupport()
        }
        var userChannels = [ChatChannel]()
        let channelDescriptor = ChannelManager.sharedManager.channelDescriptors
        channelDescriptor?.forEach({ (channelDescription) in
            let item = channelDescription as? TCHChannelDescriptor
            _ = MessagingManager.sharedManager().client
            let channel = ChannelManager.sharedManager.channelsList?.subscribedChannels().first(where: {item?.sid == $0.sid})
            let attribures = ChannelInfoAttrtibutes(attributes: channel?.attributes()?.dictionary ?? item?.attributes()?.dictionary)
            var chatChannel = ChatChannel(channelDescription: item,
                                          userDescription: nil,
                                          title: ChannelManager.sharedManager.getChannelTitle(channel: item) ?? "----",
                                          isFavorite: false, //isFavorite,
                                          photoUrl: ChannelManager.sharedManager.getAvatar(channel: item),
                                          uniqueName: item?.uniqueName,
                                          channelType: attribures.type ?? .group,
                                          channel: channel)
            if attribures.type == ChannelType.support && currentUser?.supportManager != true {
                chatChannel.title = String.lc_chatSupportTitle
                supportChannel = chatChannel
            } else if attribures.type == ChannelType.support && currentUser?.supportManager == true {
                chatChannel.title =  chatChannel.title  + "(\(String.lc_chatSupportTitle))"
                userChannels.append(chatChannel)
            } else if item?.friendlyName == ChannelManager.defaultChannelName  {
                
            } else  {
                userChannels.append(chatChannel)
            }
            
        })
        
        let userSortedChannels = userChannels.sorted(by: {$0.channelDescription?.dateUpdated ?? Date() > $1.channelDescription?.dateUpdated ?? Date()})
        var favoriteSortedChannels = userChannels.sorted(by: {$0.channelDescription?.messagesCount() ?? 0 > $1.channelDescription?.messagesCount() ?? 0})
        if let support = supportChannel {
            favoriteSortedChannels.insert(support, at: 0)
        }
        self.presenter?.handleChannels(channels: userSortedChannels)
        self.presenter?.handleFavoriteChannels(channels: favoriteSortedChannels)
    }
    
    func contactWithSupport() {
        if let manager = ChannelManager.sharedManager.supportManager {
            ChannelManager.sharedManager.chatWithSupport(userIdentity: manager.sid) { (channel) in
            }
        }
    }
 
}
