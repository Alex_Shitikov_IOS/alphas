//
//  ALChatContactsWorker.swift
//  Alpha
//
//  Created by alexandr shitikov on 03.10.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.

import UIKit
import AVFoundation


class ALChatContactsWorker {
    

    static var player: AVAudioPlayer?

    func playSound() {
        guard let url = Bundle.main.url(forResource: "insight-578", withExtension: "mp3") else { return }

        do {
            try AVAudioSession.sharedInstance().setCategory(.ambient, mode: .default)
            try AVAudioSession.sharedInstance().setActive(true)
            ALChatContactsWorker.player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)
            guard let player = ALChatContactsWorker.player else { return }

            player.play()

        } catch let error {
            print(error.localizedDescription)
        }
    }
  
}
