//
//  ALChatContactsViewController.swift
//  Alpha
//
//  Created by alexandr shitikov on 03.10.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.


import UIKit

protocol ALChatContactsDisplayLogic: class {
    func handleFavoriteChannels(channels:[ChatChannel])
    func handleChannels(chanels:[ChatChannel])
}

class ALChatContactsViewController: ALViewController, ALChatContactsDisplayLogic, UISearchResultsUpdating {
    
    var interactor: ALChatContactsBusinessLogic?
    var router: (NSObjectProtocol & ALChatContactsRoutingLogic & ALChatContactsDataPassing)?
    
    let contactsMainCellId = "ALContactWithMessageCell"
    let searchController = UISearchController(searchResultsController: nil)
    
    var dataArray = [ChatChannel]()
    var searchArray = [ChatChannel]()
    
    
    @IBOutlet weak var favoriteView: ALFavoriteChatsView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: Object lifecycle
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: Setup
    
    private func setup() {
        let viewController = self
        let interactor = ALChatContactsInteractor()
        let presenter = ALChatContactsPresenter()
        let router = ALChatContactsRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    // MARK: View lifecycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.startLoader()
        ChannelManager.sharedManager.populateChannelDescriptors()
    }
    
    override func configureUi() {
        super.configureUi()
        tableView.delegate = self
        tableView.dataSource = self
        ChannelManager.sharedManager.delegate = self
        let addChannelButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(goToSearch))
        addChannelButton.tintColor = .black
        view.backgroundColor = .navigationGray
        navigationItem.rightBarButtonItem = addChannelButton
        headerView.backgroundColor = .clear
        favoriteView.backgroundColor = .clear
        tableView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        tableView.layer.cornerRadius = 18
        tableView.register(UINib(nibName: contactsMainCellId, bundle: nil), forCellReuseIdentifier: contactsMainCellId)
        tableView.tableFooterView = UIView()
        navigationItem.title = String.lc_chatMainTitle
        searchController.searchBar.backgroundColor = .navigationGray
        searchController.searchBar.backgroundImage = UIImage()
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = ""
        searchController.searchBar.keyboardAppearance = .light
        searchController.searchBar.searchTextField.textColor = .mainTextBlack
        searchController.searchBar.tintColor = .mainTextBlack
        navigationItem.searchController = searchController
        definesPresentationContext = false
        tabBarItem.title = ""
        favoriteView.delegate = self
        view.startLoader()
    }
    
    @objc func goToSearch() {
        router?.navigateToSearch()
    }
    
}
