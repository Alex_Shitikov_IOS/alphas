//
//  ALChatContactsControllerDelegate.swift
//  Alpha
//
//  Created by alexandr shitikov on 03.10.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.

import UIKit
import TwilioChatClient

extension ALChatContactsViewController: ChannelManagerDelegate, UITableViewDelegate, UITableViewDataSource, ALFavoriteChatsViewDelegate {
    
    //MARK: ALChatContactsDisplayLogic
    
    func handleFavoriteChannels(channels:[ChatChannel]) {
        favoriteView.configure(contacts: channels)
    }
    
    func handleChannels(chanels:[ChatChannel]) {
        view.stopLoader()
        tableView.hidePaceholder()
        dataArray.removeAll()
        dataArray = chanels
        updateTableWithSearchParams()
    }
    
    //MARK: UISearchResultsUpdating
    
    func updateSearchResults(for searchController: UISearchController) {
        updateTableWithSearchParams()
    }

    //MARK: ChannelManagerDelegate
    
    func reloadChannelDescriptorList() {
        self.interactor?.getChannels()

    }
    
    //MARK: UITableViewDataSource
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:contactsMainCellId) as! ALContactWithMessageCell
        let item = searchArray[indexPath.item]
        cell.configure(item: item)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = searchArray[indexPath.item]
        view.startLoader()
        item.channelDescription?.channel(completion: { (result, channel) in
            MessagingManager.sharedManager().joinChannel(channel: channel) { (isSuccess) in
                self.view.stopLoader()
                if isSuccess {
                    self.router?.navigateToChat(channel: channel!, chatChannel: item)
                }
            }
        })
    }
 
    //MARK: ALFavoriteChatsViewDelegate
    func chatDidSelect(chat: ChatChannel) {
        chat.channelDescription?.channel(completion: { (result, channel) in
            MessagingManager.sharedManager().joinChannel(channel: channel) { (isSuccess) in
                self.view.stopLoader()
                if isSuccess {
                    self.router?.navigateToChat(channel: channel!, chatChannel: chat)
                }
            }
        })
    }
    
    func updateTableWithSearchParams() {
        if searchController.searchBar.text?.isEmpty ?? true {
            searchArray.removeAll()
            searchArray = dataArray
        } else {
            searchArray.removeAll()
            searchArray = dataArray.all(where: {$0.title.lowercased().contains(searchController.searchBar.text!.lowercased())})
        }
        if searchArray.isEmpty {
            tableView.showPaceholder(type: .noContacts)
        } else {
            tableView.hidePaceholder()
        }
        tableView.reloadData()
    }
}
