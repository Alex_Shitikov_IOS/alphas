//
//  ALContactWithMessageCell.swift
//  Alpha
//
//  Created by alexandr shitikov on 17.10.2020.
//  Copyright © 2020 alexandr shitikov. All rights reserved.
//

import UIKit
import TwilioChatClient

class ALContactWithMessageCell: UITableViewCell {
    
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var newMessageIndicator: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        avatarImage.rounded(radius: 8)
        userNameLabel.textColor = .mainTextBlack
        messageLabel.textColor = .mainTextBlack
        timeLabel.textColor = .textGray
        messageLabel.text = ""
        userNameLabel.text = ""
        timeLabel.text = ""
        newMessageIndicator.isHidden = true
    }
    
    func configure(item:ChatChannel) {
        timeLabel.text = ""
        messageLabel.text = ""
        userNameLabel.text = item.title
        avatarImage.image = #imageLiteral(resourceName: "photo")
        newMessageIndicator.isHidden = true
        TempDataManager.shared.getPhoto(url: item.photoUrl) { (image) in
            if let photo = image {
                self.avatarImage.image = photo
            }
        }
        
        // configure message
        item.channel?.messages?.getLastWithCount(1, completion: { (result, messageList) in
            if let message = messageList?.last {
                self.messageLabel.text = message.body
                let member = item.channel?.members?.membersList().first(where: {$0.identity == MessagingManager.sharedManager().client?.user?.identity})
                if message.author != member?.identity {
                    item.channel?.getUnconsumedMessagesCount(completion: { (result, count) in
                        if Int(truncating: count ?? 0) > 0 {
                            self.newMessageIndicator.isHidden = false
                        }
                    })
                }
               
            }
        })
        
        //configure time
        if let date = item.channel?.lastMessageDate {
            timeLabel.text = date.string(withFormat: .HH_mm)
        }
        
    }
    
    

   
    
}
