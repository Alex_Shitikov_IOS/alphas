//
//  ALContactsMainCell.swift
//  Alpha
//
//  Created by alexandr shitikov on 06.10.2020.
//  Copyright © 2020 alexandr shitikov. All rights reserved.
//

import UIKit

class ALContactsMainCell: UITableViewCell {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
        iconImageView.rounded(radius: 8)
    }
    
    func configure(channel:ChatChannel) {
        titleLabel.text = channel.title
        self.iconImageView.image = #imageLiteral(resourceName: "photo")
        TempDataManager.shared.getPhoto(url: channel.photoUrl) { (image) in
            if let photo = image {
                self.iconImageView.image = photo
            }
        }
        
    }

   
    
}
