//
//  ALChatContactsPresenter.swift
//  Alpha
//
//  Created by alexandr shitikov on 03.10.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.


import UIKit

protocol ALChatContactsPresentationLogic {
    func handleFavoriteChannels(channels:[ChatChannel])
    func handleChannels(channels:[ChatChannel])
}

class ALChatContactsPresenter: ALChatContactsPresentationLogic
{
  weak var viewController: ALChatContactsDisplayLogic?
  
  // MARK: ALChatContactsPresentationLogic
    
    func handleFavoriteChannels(channels:[ChatChannel]) {
        viewController?.handleFavoriteChannels(channels: channels)
    }
    
    func handleChannels(channels:[ChatChannel]) {
        viewController?.handleChannels(chanels: channels)
    }
  
}
