//
//  ALChatContactsRouter.swift
//  Alpha
//
//  Created by alexandr shitikov on 03.10.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.

import UIKit
import TwilioChatClient

protocol ALChatContactsRoutingLogic {
    func navigateToChat(channel:TCHChannel, chatChannel:ChatChannel)
    func navigateToSearch()
}

protocol ALChatContactsDataPassing
{
    var dataStore: ALChatContactsDataStore? { get }
}

class ALChatContactsRouter: NSObject, ALChatContactsRoutingLogic, ALChatContactsDataPassing {
    weak var viewController: ALChatContactsViewController?
    var dataStore: ALChatContactsDataStore?
    private let chatId = "ALMainChatViewController"
    private let contactsSearchId = "ALContactsSearchViewController"
    
    // MARK: Routing
    
    func navigateToChat(channel:TCHChannel, chatChannel:ChatChannel) {
        DispatchQueue.main.async {
            let controller = ALStoryboard.Chat.initController(withId: self.chatId) as! ALMainChatViewController
            controller.channel = channel
            controller.chatChannel = chatChannel
            let navController = UINavigationController(rootViewController: controller)
            navController.modalPresentationStyle = .fullScreen
            AppDelegate.topViewController()?.present(navController, animated: true, completion: nil)
        }
    }
    
    func navigateToSearch() {
        DispatchQueue.main.async {
            let controller = ALStoryboard.Chat.initController(withId: self.contactsSearchId)
            let navController = ALNavigationController(rootViewController: controller)
            navController.modalPresentationStyle = .fullScreen
            self.viewController?.present(navController, animated: true, completion: nil)
        }
    }
}
