//
//  ALChatNavigationController.swift
//  Alpha
//
//  Created by alexandr shitikov on 07.10.2020.
//  Copyright © 2020 alexandr shitikov. All rights reserved.
//

import UIKit

class ALChatNavigationController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 13.0, *) {
            let navBarAppearance = UINavigationBarAppearance()
            navBarAppearance.configureWithTransparentBackground()
            navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.black]
            navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.black]
            navigationBar.standardAppearance = navBarAppearance
            navigationBar.scrollEdgeAppearance = navBarAppearance
        }
        
        navigationBar.isTranslucent = false
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.navigationGray]
        navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationBar.shadowImage = UIImage()
        self.view.backgroundColor = UIColor.navigationGray
        self.navigationBar.backgroundColor = .navigationGray
        navigationBar.barTintColor = UIColor.green
        
    }
    
}
