//
//  ALNavigationView.swift
//  Alpha
//
//  Created by alexandr shitikov on 15.10.2020.
//  Copyright © 2020 alexandr shitikov. All rights reserved.
//

import UIKit

class ALNavigationView: UIView {

    @IBOutlet var mainView: UIView!
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("ALNavigationView", owner: self, options: nil)
        addSubview(mainView)
        mainView.frame = self.bounds
        mainView.clipsToBounds = true
        mainView.backgroundColor = .clear
        titleLabel.text = ""
        titleLabel.textColor = .mainTextBlack
        photo.rounded(radius: 4)
    }
}
