//
//  ALMainChatRouter.swift
//  Alpha
//
//  Created by alexandr shitikov on 04.10.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.

import UIKit
import TwilioChatClient

protocol ALMainChatRoutingLogic {
    func navigateToSettings(channel: TCHChannel?)
}

protocol ALMainChatDataPassing
{
  var dataStore: ALMainChatDataStore? { get }
}

class ALMainChatRouter: NSObject, ALMainChatRoutingLogic, ALMainChatDataPassing
{
  weak var viewController: ALMainChatViewController?
  var dataStore: ALMainChatDataStore?
    let settingsControllerId = "ALEditGroupViewController"
  
  // MARK: Routing
    
    func navigateToSettings(channel: TCHChannel?) {
        let controller = ALStoryboard.Chat.initController(withId: settingsControllerId) as! ALEditGroupViewController
        controller.channel = channel
        viewController?.navigationController?.pushViewController(controller, animated: true)
    }
}
