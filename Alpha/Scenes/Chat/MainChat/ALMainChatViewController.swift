//
//  ALMainChatViewController.swift
//  Alpha
//
//  Created by alexandr shitikov on 04.10.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.


import UIKit
import TwilioChatClient
import MessageKit
import InputBarAccessoryView

protocol ALMainChatDisplayLogic: class {
    
}

class ALMainChatViewController: MessagesViewController, ALMainChatDisplayLogic, InputBarAccessoryViewDelegate, ALPhotoPickerDelegate {

    var interactor: ALMainChatBusinessLogic?
    var router: (NSObjectProtocol & ALMainChatRoutingLogic & ALMainChatDataPassing)?
    
    var channel: TCHChannel?
    var chatChannel: ChatChannel?
    var messages: Set<TCHMessage> = Set<TCHMessage>()
    var sortedMessages = [TCHMessage]()
    var navigationView: ALNavigationView?
    var cashPhotos = [PhotoCashItem]()
    var fileSelector: CLPhotoPicker = CLPhotoPicker()
    private let messageCount: UInt = 100
    let labelHeight: CGFloat = 17

    // MARK: Object lifecycle
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: Setup
    
    private func setup() {
        let viewController = self
        let interactor = ALMainChatInteractor()
        let presenter = ALMainChatPresenter()
        let router = ALMainChatRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationUI()
        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messagesLayoutDelegate = self
        messagesCollectionView.messagesDisplayDelegate = self
        messagesCollectionView.clipsToBounds = true
        messagesCollectionView.layer.masksToBounds = true
        loadMessages()
        channel?.delegate = self
        scrollsToBottomOnKeyboardBeginsEditing = true // default false
        maintainPositionOnKeyboardFrameChanged = true // default false
        showMessageTimestampOnSwipeLeft = true
        configureMessageInputBar()
        addBackButton()
        navigationController?.navigationBar.backgroundColor = .navigationGray
        navigationController?.navigationBar.barTintColor = .navigationGray
        view.backgroundColor = .navigationGray
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.black]
        if chatChannel?.channelType == ChannelType.group {
            let settingButton = UIBarButtonItem(image: #imageLiteral(resourceName: "settings"), style: .plain, target: self, action: #selector(navigateToSettings))
            settingButton.tintColor = .orangeButton
            navigationItem.rightBarButtonItem = settingButton
        }
        let imageView = UIImageView(frame: messagesCollectionView.frame)
        imageView.contentMode = .scaleAspectFill
        imageView.image = UIImage(named: "chatBackground")
        messagesCollectionView.backgroundView = imageView
        showMessageTimestampOnSwipeLeft = false
        navigationItem.backButtonTitle = ""
        navigationController?.navigationBar.tintColor = .black
        fileSelector.target = self
        fileSelector.delegate = self
    }
    
    func configureMessageInputBar() {
        messageInputBar.delegate = self
        messageInputBar.inputTextView.tintColor = .mainTextBlack
        messageInputBar.sendButton.setTitleColor(.orangeButton, for: .normal)
        messageInputBar.sendButton.setTitleColor(
            UIColor.orangeButton.withAlphaComponent(0.3), for: .highlighted
        )
        messageInputBar.sendButton.tintColor = .orangeButton
        messageInputBar.inputTextView.layer.borderWidth = 1
        messageInputBar.inputTextView.layer.borderColor = UIColor.orangeButton.cgColor
        messageInputBar.inputTextView.layer.masksToBounds = true
        messageInputBar.sendButton.imageView?.backgroundColor = .clear
        messageInputBar.sendButton.image = #imageLiteral(resourceName: "sendMessage")
        messageInputBar.sendButton.title = nil
    }
    
    @objc func openGalary() {
        fileSelector.chosePhoto()
    }
    
    //MARK: ALPhotoPickerDelegate
    
    func fileIsSelected(info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            sendImage(image: pickedImage)
        }
    }

    // MARK: - Chat Service
    
    func sendMessage(inputMessage: String) {
        let messageOptions = TCHMessageOptions().withBody(inputMessage)
        channel?.messages?.sendMessage(with: messageOptions, completion: nil)
    }
    
    func sendImage(image:UIImage) {
        interactor?.sendImage(image: image)
    }
    
    func addMessages(newMessages:Set<TCHMessage>) {
        messages =  messages.union(newMessages)
        sortMessages()
        DispatchQueue.main.async {
            self.messagesCollectionView.reloadData()
            if !self.messages.isEmpty{
                self.scrollToBottom()
            }
        }
    }
    
    func sortMessages() {
        sortedMessages = messages.sorted { (a, b) -> Bool in
            (a.dateCreated ?? "") < (b.dateCreated ?? "")
        }
    }
    
    func loadMessages() {
        messages.removeAll()
        if channel?.synchronizationStatus == .all {
            channel?.messages?.getLastWithCount(messageCount) { (result, items) in
                self.addMessages(newMessages: Set(items!))
                self.updateLastConsumedMessageIndex(message: items?.last, channel: self.channel)
            }
        }
    }
    
    func scrollToBottom() {
        messagesCollectionView.scrollToLastItem()
    }
    
    //MARK: Actions
    
    func addBackButton() {
        let navButton = UIBarButtonItem(image: #imageLiteral(resourceName: "chevron-left"), style: .plain, target: self, action: #selector(backAction))
        navButton.tintColor = .black
        self.navigationItem.setLeftBarButton(navButton, animated: false)
    }
    
    @objc func backAction() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func navigateToSettings() {
        router?.navigateToSettings(channel: channel)
    }
   
}
