//
//  ALMainChatPresenter.swift
//  Alpha
//
//  Created by alexandr shitikov on 04.10.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.


import UIKit

protocol ALMainChatPresentationLogic {
    
}

class ALMainChatPresenter: ALMainChatPresentationLogic
{
  weak var viewController: ALMainChatDisplayLogic?
  
  // MARK: ALMainChatPresentationLogic
  
}
