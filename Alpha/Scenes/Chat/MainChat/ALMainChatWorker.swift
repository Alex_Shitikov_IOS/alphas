//
//  ALMainChatWorker.swift
//  Alpha
//
//  Created by alexandr shitikov on 04.10.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.

import UIKit
import TwilioChatClient
import MessageKit

class ALMainChatWorker {
    
    static func favorite(channelId:String, isFavoriate:Bool) {
        UserDefaults.standard.set(isFavoriate, forKey: channelId)
    }
    
    static func isFavoriate(channelId:String?) -> Bool {
        if let key = channelId {
            if (UserDefaults.standard.value(forKey: key) ?? false) as! Bool == true {
                return true
            }
        }
        return false
    }
    
    func getUserInfo(uniqueName:String, completion:@escaping(TCHUserDescriptor?) -> Void) {
        let member = ChannelManager.sharedManager.generalChannel.member(withIdentity: uniqueName)
        member?.userDescriptor(completion: { (result, userDescription) in
            completion(userDescription)
        })
    }
    
}
