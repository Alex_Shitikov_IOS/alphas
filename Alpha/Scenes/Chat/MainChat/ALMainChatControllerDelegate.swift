//
//  ALMainChatControllerDelegate.swift
//  Alpha
//
//  Created by alexandr shitikov on 04.10.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.

import UIKit
import MapKit
import MessageKit
import TwilioChatClient
import InputBarAccessoryView

extension ALMainChatViewController: MessagesDisplayDelegate, MessagesDataSource, TCHChannelDelegate, MessagesLayoutDelegate {
    
    //MARK: MessagesLayoutDelegate
    
    func textColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        return .mainTextBlack
    }
    
    func messageTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        return NSAttributedString(string: MessageKitDateFormatter.shared.string(from: message.sentDate), attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 10), NSAttributedString.Key.foregroundColor: UIColor.textGray])
    }
    
    func cellBottomLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        if currentSender().senderId == message.sender.senderId {
            let item = convert(sortedMessages[indexPath.item])
            return NSAttributedString(string: getReadStatusTitle(message: item.message), attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 10), NSAttributedString.Key.foregroundColor: UIColor.textGray])
        } else {
            return NSAttributedString(string: message.sender.displayName, attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 10), NSAttributedString.Key.foregroundColor: UIColor.textGray])
        }
    }
    
    func cellTopLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return labelHeight
    }
    
    func cellBottomLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return labelHeight
    }
    
    func messageTopLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return labelHeight
    }
    
    func messageBottomLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return labelHeight
    }
    
    func configureAvatarView(_ avatarView: AvatarView, for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) {
        let message = convert(sortedMessages[indexPath.item])
        TempDataManager.shared.getPhoto(url: message.info?.photoUrl) { (image) in
            if let photo = image {
                let shortName = String(message.sender.displayName.prefix(1))
                let avatar = Avatar(image: photo, initials: shortName)
                avatarView.set(avatar: avatar)
            }
        }
    }
    
    func backgroundColor(for message: MessageType, at indexPath: IndexPath,
                         in messagesCollectionView: MessagesCollectionView) -> UIColor {
        return isFromCurrentSender(message: message) ? .currentUserMessage : .outsideMessage
    }
    
    func shouldDisplayHeader(for message: MessageType, at indexPath: IndexPath,
                             in messagesCollectionView: MessagesCollectionView) -> Bool {
        return true
    }
    
    func messageStyle(for message: MessageType, at indexPath: IndexPath,
                      in messagesCollectionView: MessagesCollectionView) -> MessageStyle {
        let corner: MessageStyle.TailCorner = isFromCurrentSender(message: message) ? .bottomRight : .bottomLeft
        return .bubbleTail(corner, .curved)
    }
    
    //MARK: MessagesDataSource
    
    func currentSender() -> SenderType {
        let user = APIManager.manager.currentUser
        let userName = (user?.firstName ?? "?") + (" \(user?.lastName ?? "?")")
        let client = MessagingManager.sharedManager().client
        return ChatUser(senderId: client?.user?.identity ?? "---",
                        displayName: userName,
                        avatarUrl: user?.photo)
    }
    
    func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
        let item = sortedMessages[indexPath.item]
        return convert(item)
    }
    
    func numberOfItems(inSection section: Int, in messagesCollectionView: MessagesCollectionView) -> Int {
        return messages.count
    }
    
    func numberOfSections(in messagesCollectionView: MessagesCollectionView) -> Int {
        return 1
    }
    
    //MARK: TCHChannelDelegate
    
    func chatClient(_ client: TwilioChatClient, channel: TCHChannel, messageAdded message: TCHMessage) {
        if !messages.contains(message) {
            addMessages(newMessages: [message])
            ALChatContactsWorker().playSound()
        }
        updateLastConsumedMessageIndex(message: message, channel: channel)
    }
    
    func chatClient(_ client: TwilioChatClient, channel: TCHChannel, member: TCHMember, updated: TCHMemberUpdate) {
        self.messagesCollectionView.reloadData()
    }
    
    func chatClient(_ client: TwilioChatClient, channel: TCHChannel, memberJoined member: TCHMember) {
        addMessages(newMessages: [StatusMessage(statusMember:member, status:.Joined)])
    }
    
    func chatClient(_ client: TwilioChatClient, channel: TCHChannel, memberLeft member: TCHMember) {
        addMessages(newMessages: [StatusMessage(statusMember:member, status:.Left)])
    }
    
    func chatClient(_ client: TwilioChatClient, channelDeleted channel: TCHChannel) {
        DispatchQueue.main.async {
            if channel == self.channel {
                let data = AlertData(title: String.lc_chatOwnerDeletedChat,
                                     message: "",
                                     type: .error)
                ALAlertViewController.show(data: data,
                                           target: self,
                                           action: #selector(self.closeChatAction))
            }
        }
    }
    
    func chatClient(_ client: TwilioChatClient,
                    channel: TCHChannel,
                    synchronizationStatusUpdated status: TCHChannelSynchronizationStatus) {
        if status == .all {
            loadMessages()
            DispatchQueue.main.async {
                self.messagesCollectionView.reloadData()
            }
        }
    }
    
    //MARK: InputBarAccessoryViewDelegate
    
    func inputBar(_ inputBar: InputBarAccessoryView, didPressSendButtonWith text: String) {
        sendMessage(inputMessage: text)
        inputBar.inputTextView.text = ""
    }
    
    //MARK: Helpers
    
    @objc func closeChatAction() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func setupNavigationUI() {
        guard let navigationBar = self.navigationController?.navigationBar else { return }
        navigationView = ALNavigationView(frame: CGRect(x: 0, y: 0, width: navigationBar.frame.width, height: 40))
        navigationBar.addSubview(navigationView!)
        navigationView?.center = navigationBar.center
        navigationView?.titleLabel.text = chatChannel?.title
        if chatChannel?.channelType == ChannelType.support {
            self.navigationView?.photo.image = #imageLiteral(resourceName: "supportIcon")
        } else {
            APIManager.manager.getPhoto(url: chatChannel?.photoUrl) { (image) in
                if let photo = image {
                    self.navigationView?.photo.image = photo
                }
            }
        }
    }
    
    func updateLastConsumedMessageIndex(message:TCHMessage?, channel:TCHChannel?) {
        if message?.author != MessagingManager.sharedManager().client?.user?.identity {
            if let newIndex = message?.index {
                channel?.messages?.setLastConsumedMessageIndex(newIndex, completion: { (result, val) in
                    if result.isSuccessful() {
                        self.messagesCollectionView.reloadData()
                    }
                })
            }
        }
    }
    
    func getReadStatusTitle(message:TCHMessage) -> String {
        if message.author == MessagingManager.sharedManager().client?.user?.identity {
            guard let newIndex = message.index else {
                return String.lc_chatMessageStatusDelivered
            }
            if Int(truncating: newIndex) > getMaxReadIndex() {
                return String.lc_chatMessageStatusDelivered
            } else {
                return String.lc_chatMessageStatusRead
            }
        }
        return ""
    }
    
    func getMaxReadIndex() -> Int {
        var index = -1
        channel?.members?.membersList().forEach({ (member) in
            guard let newIndex = member.lastConsumedMessageIndex else {
                return
            }
            if Int(truncating: newIndex) > index {
                index = Int(truncating: newIndex)
            }
        })
        return index
    }
    
    func convert(_ TCH:TCHMessage) -> ChatMessage {
        let author = ChannelManager.sharedManager.users.first(where: {$0.identity == TCH.author})
        let attribures = UserInfoAttributes(attributes: author?.attributes()?.dictionary)
        let sender = ChatUser(senderId: TCH.author ?? "", displayName: author?.friendlyName ?? "??", avatarUrl: attribures.photoUrl)
        let message = ChatMessage(sender: sender,
                                  messageId: TCH.sid ?? "",
                                  sentDate: TCH.dateCreatedAsDate ?? Date(),
                                  kind: getUIMediaType(message: TCH), //MessageKind.text(TCH.body ?? ""),
                                  message: TCH,
                                  info: attribures)
        return message
    }
    
    func getUIMediaType(message:TCHMessage) -> MessageKind {
        if message.mediaType == nil {
            return MessageKind.text(message.body ?? "")
        } else if message.mediaType == "image/jpeg" {
            let item = ALMediaItem(url: nil, image: getPhoto(message: message), placeholderImage: #imageLiteral(resourceName: "photo"), size: CGSize(width: 300, height: 200))
            return MessageKind.photo(item)
        }
        return MessageKind.text(message.body ?? "")
    }
    
    func getPhoto(message:TCHMessage) -> UIImage? {
        if let item = cashPhotos.first(where: {$0.messageId == message.sid}) {
            return item.image
        } else {
            self.getMedia(message: message)
        }
        return nil
    }
    
    func getMedia(message:TCHMessage) {
        let item = PhotoCashItem(messageId: message.sid!, image: nil)
        self.cashPhotos.append(item)
        
        message.getMediaContentTemporaryUrl { (result, url) in
            if result.isSuccessful() {
                APIManager.manager.getPhoto(url: url) { (image) in
                    if let index = self.cashPhotos.firstIndex(where: {$0.messageId == message.sid}) {
                        self.cashPhotos[index].image = image
                    } else {
                        self.cashPhotos.append(item)
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                        guard let index = self.sortedMessages.firstIndex(where: {$0.sid == message.sid})
                        else {return}
                        let indexPath = IndexPath(item: index, section: 0)
                        self.messagesCollectionView.reloadItems(at: [indexPath])
                    }
                }
            }
        }
    }
    
}
