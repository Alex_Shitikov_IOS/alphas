//
//  ALMainChatInteractor.swift
//  Alpha
//
//  Created by alexandr shitikov on 04.10.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.
//
//

import UIKit
import TwilioChatClient
import MessageKit

protocol ALMainChatBusinessLogic {
    func sendImage(image:UIImage)
}

protocol ALMainChatDataStore {
  //var name: String { get set }
}

class ALMainChatInteractor: ALMainChatBusinessLogic, ALMainChatDataStore {
  var presenter: ALMainChatPresentationLogic?
  var worker: ALMainChatWorker?
  
  // MARK: ALMainChatBusinessLogic
    
    func sendImage(image:UIImage) {
        // The data for the image you would like to send
        let cropedImage = image.resizeImage(newWidth: 300)
        
        guard let data = cropedImage?.jpegData(compressionQuality: 0.5) else {return}
        
        // Prepare the upload stream and parameters
        let messageOptions = TCHMessageOptions()
        let inputStream = InputStream(data: data)
        messageOptions.withMediaStream(inputStream,
                                       contentType: "image/jpeg",
                                       defaultFilename: "image.jpg") {
            print("started")
        } onProgress: { (progress) in
            print(progress)
        } onCompleted: { (result) in
            print(result)
        }
    }
    
}
