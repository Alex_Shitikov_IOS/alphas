//
//  ALMainChatModels.swift
//  Alpha
//
//  Created by alexandr shitikov on 04.10.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.


import UIKit
import MessageKit
import TwilioChatClient

struct ChatUser: SenderType, Codable {
    var senderId: String
    var displayName: String
    var avatarUrl:String?
}

struct MessageRequest: Codable {
    let user:ChatUser
    let message:String
    
    init(user:ChatUser, message:String) {
        self.user = user
        self.message = message
    }
}


struct ChatMessage: MessageType {
    var sender: SenderType
    var messageId: String
    var sentDate: Date
    var kind: MessageKind
    var message:TCHMessage
    var info:UserInfoAttributes?
}

struct ChatChannel {
    let channelDescription:TCHChannelDescriptor?
    let userDescription:TCHUserDescriptor?
    var title:String
    let isFavorite:Bool
    let photoUrl:String?
    let uniqueName:String?
    let channelType:ChannelType
    let channel:TCHChannel?
}

struct ALMediaItem:MediaItem {
    var url: URL?
    var image: UIImage?
    var placeholderImage: UIImage
    var size: CGSize
}

struct PhotoCashItem {
    let messageId:String
    var image:UIImage?
}

struct ChannelInfoAttrtibutes {
    let type: ChannelType?
    let userId: String?
    var photo:String?
    
    init(attributes:Dictionary<AnyHashable, Any>?) {
        let typeVal = attributes?[ChannelInfoAttrtibutes.CodingKeys.type.rawValue] as? String
        let userId = attributes?[ChannelInfoAttrtibutes.CodingKeys.userId.rawValue] as? String
        let photo = attributes?[ChannelInfoAttrtibutes.CodingKeys.photo.rawValue] as? String
        self.type = ChannelType.init(rawValue: typeVal ?? ChannelType.group.rawValue) ?? ChannelType.none
        self.userId = userId
        self.photo = photo
    }

    enum CodingKeys: String, CodingKey {
        case type = "type"
        case userId = "userId"
        case photo = "photo"
    }
}

struct UserInfoAttributes {
    let photoUrl:String?
    
    init(attributes:Dictionary<AnyHashable, Any>?) {
        self.photoUrl = attributes?["photo"] as? String
    }
}

struct ChatMember {
    let user:TCHUser?
}


enum ALMainChat {
  // MARK: Use cases
  
}

enum ChannelType: String {
    case user = "USER"
    case group = "GROUP"
    case support = "SUPPORT"
    case none = "NONE"
}

// MARK: - SupportManager
struct SupportManager: Codable {
    let sid: String
    let serviceSid: String

    enum CodingKeys: String, CodingKey {
        case sid = "sid"
        case serviceSid = "serviceSid"
    }
}
