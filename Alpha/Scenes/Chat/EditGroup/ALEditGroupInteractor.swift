//
//  ALEditGroupInteractor.swift
//  Alpha
//
//  Created by alexandr shitikov on 18.10.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.
//

import UIKit
import TwilioChatClient

protocol ALEditGroupInteractorInputProtocol {
    func getUserList(channel: TCHChannel)
    func savePhoto(image: UIImage)
    func saveChannel(name: String?, url: String?, channelSid: String?)
}

protocol ALEditGroupInteractorOutputProtocol {
    
}

class ALEditGroupInteractor: ALEditGroupInteractorInputProtocol, ALEditGroupInteractorOutputProtocol {
    
    var presenter: ALEditGroupPresentationLogic?
    var worker: ALEditGroupWorker?
    let workGroup = DispatchGroup()
    
    // MARK: ALEditGroupInteractorInputProtocol
    
    func getUserList(channel: TCHChannel) {
        var users = [ChatChannel]()
        channel.members?.membersList().forEach({ (member) in
            workGroup.enter()
            member.userDescriptor { (result, userDescription) in
                let attributes = UserInfoAttributes(attributes: userDescription?.attributes()?.dictionary)
                let user = ChatChannel(channelDescription: nil,
                                       userDescription: userDescription,
                                       title: userDescription?.friendlyName ?? "unknown",
                                       isFavorite: false,
                                       photoUrl:attributes.photoUrl,
                                       uniqueName: userDescription?.identity ?? "",
                                       channelType: .user,
                                       channel: nil)
                users.append(user)
                
                self.workGroup.leave()
            }
        })
        
        workGroup.notify(queue: .main) {
            self.presenter?.handleChannels(channels: users)
        }
    }
    
    func savePhoto(image: UIImage) {
        let url = PCURLs.File.upload.rawValue
        APIManager.manager.uploadImage(url: url, params: nil, image: image) { (data, response, error) in
            DispatchQueue.main.async {
                let imageUrl = String(data: data ?? Data(), encoding: .utf8)
                self.presenter?.handlePhotoUpload(url: imageUrl)
            }
        }
    }
    
    func saveChannel(name: String?, url: String?, channelSid: String?) {
        let channel = ChannelManager.sharedManager.channelsList?.subscribedChannels().first(where: {$0.sid == channelSid})
        channel?.setFriendlyName(name, completion: { (result) in
            print(result)
        })
        if let photoUrl = url {
            let attributes = ChannelInfoAttrtibutes(attributes: channel?.attributes()?.dictionary)
            
            let newAttributes: TCHJsonAttributes = TCHJsonAttributes(dictionary:
                                                                        [ChannelInfoAttrtibutes.CodingKeys.type.rawValue: attributes.type?.rawValue ?? ChannelType.group.rawValue,
                                                                         ChannelInfoAttrtibutes.CodingKeys.photo.rawValue: photoUrl])
            channel?.setAttributes(newAttributes, completion: { (result) in
                print(result)
            })
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.presenter?.handleSaveSuccess()
        }
    }
}
