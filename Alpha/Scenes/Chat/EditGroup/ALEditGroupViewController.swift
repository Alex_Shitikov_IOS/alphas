//
//  ALEditGroupViewController.swift
//  Alpha
//
//  Created by alexandr shitikov on 18.10.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.
//


import UIKit
import TwilioChatClient

protocol ALEditGroupViewControllerProtocol: class {
    func handleUserList(channels:[ChatChannel])
    func handlePhotoUpload(url: String?)
    func handleSaveSuccess()
}

class ALEditGroupViewController: ALViewController, ALEditGroupViewControllerProtocol {
    
    var interactor: ALEditGroupInteractorInputProtocol?
    var router: (NSObjectProtocol & ALEditGroupRoutingLogic & ALEditGroupDataPassing)?
    var channel: TCHChannel?
    var isEditingMode = false
    let contactsMainCellId = "ALContactsMainCell"
    var fileSelector: CLPhotoPicker = CLPhotoPicker()
    var photoDidChange = false
    var dataArray = [ChatChannel]()
    private let topViewHeightConst: CGFloat = 290
    let heightForRow: CGFloat = 90

    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var topViewHeight: NSLayoutConstraint! // 200 or 290
    @IBOutlet weak var nameInput: ALTextInput!
    @IBOutlet weak var photoImage: UIImageView!
    @IBOutlet weak var centerView: UIView!
    @IBOutlet weak var photoButton: UIButton!
    @IBOutlet weak var membersCountLabel: UILabel!
    @IBOutlet weak var memberButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var exitButton: UIButton!
    @IBOutlet weak var exitLabel: UILabel!
    
    // MARK: Object lifecycle
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: Setup
    
    private func setup() {
        let viewController = self
        let interactor = ALEditGroupInteractor()
        let presenter = ALEditGroupPresenter()
        let router = ALEditGroupRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }

    // MARK: View lifecycle
    
    override func configureUi() {
        super.configureUi()
        nameInput.setViewType(type: .about, tag: 1)
        nameInput.textField.placeholder = String.lc_chatGroupNamePlaceholderText
        view.backgroundColor = .white
        topView.backgroundColor = .white
        centerView.backgroundColor = .white
        memberButton.setTitleColor(.orangeButton, for: .normal)
        memberButton.layer.borderColor = UIColor.orangeButton.cgColor
        memberButton.layer.borderWidth = 1
        memberButton.setTitle(String.lc_chatAddUserButtonTitle, for: .normal)
        memberButton.setImage(#imageLiteral(resourceName: "users"), for: .normal)
        memberButton.tintColor = .orangeButton
        photoButton.rounded()
        photoButton.backgroundColor = .orangeButton
        photoButton.tintColor = .white
        membersCountLabel.textColor = .mainTextBlack
        exitLabel.text = String.lc_chatExitFromGroupTitle
        tableView.register(UINib(nibName: contactsMainCellId, bundle: nil), forCellReuseIdentifier: contactsMainCellId)
        tableView.tableFooterView = UIView()
        tableView.delegate = self
        tableView.dataSource = self
        photoButton.addTarget(self, action: #selector(openGalary), for: .touchUpInside)
        fileSelector.target = self
        fileSelector.delegate = self
        setEditingState(isEditing: false)
        setCurentInfo()
    }
    
    //MARK: Actions
    
    @IBAction func addUsersAction(_ sender: Any) {
        router?.navigateToAddUsers(channel: channel)
    }
    
    @IBAction func exitAction(_ sender: Any) {
        self.dismiss(animated: true) {
            ChannelManager.sharedManager.leaveCahnnel(channelSid: self.channel!.sid!)
        }
    }
    
    @objc func openGalary() {
        fileSelector.chosePhoto()
    }
    
    func setCurentInfo() {
        guard let currentChannel = channel else {return}
        interactor?.getUserList(channel: currentChannel)
        membersCountLabel.text = String.lc_chatMembersCount + " (\(currentChannel.members?.membersList().count ?? 0))"
        let item = ChannelInfoAttrtibutes(attributes: currentChannel.attributes()?.dictionary)
        TempDataManager.shared.getPhoto(url: item.photo) { (image) in
            if let photo = image {
                self.photoImage.image = photo
            }
        }
    }
    
    func setEditingState(isEditing:Bool) {
        isEditingMode = isEditing
        navigationItem.rightBarButtonItem = nil
        var alpha:CGFloat = 1
        if isEditing {
            nameInput.textField.text = channel?.friendlyName
            photoButton.isUserInteractionEnabled = true
            nameInput.isUserInteractionEnabled = true
            topViewHeight.constant = topViewHeightConst
            topView.remove(border: .bottom)
            topView.add(border: .bottom, color: .navigationGray, height: 8, leftOffset: 0)

            let saveButton = UIBarButtonItem(image: #imageLiteral(resourceName: "check"), style: .plain, target: self, action: #selector(saveAction))
            saveButton.tintColor = .orangeButton
            navigationItem.rightBarButtonItem = saveButton
        } else {
            alpha = 0
            photoButton.isUserInteractionEnabled = false
            nameInput.isUserInteractionEnabled = false
            topViewHeight.constant = topViewHeightConst
            topView.remove(border: .bottom)
            topView.add(border: .bottom, color: .navigationGray, height: 8, leftOffset: 0)
            let editButton = UIBarButtonItem(image: #imageLiteral(resourceName: "edit-2"), style: .plain, target: self, action: #selector(editAction))
            editButton.tintColor = .orangeButton
            navigationItem.rightBarButtonItem = editButton
        }
        UIView.animate(withDuration: 0.6) {
            self.view.layoutSubviews()
            self.photoButton.alpha = alpha
            self.nameInput.alpha = alpha
        }
    }
    
    @objc func saveAction() {
        if nameInput.isValid() {
            view.startLoader()
            if photoDidChange == true {
                interactor?.savePhoto(image: photoImage.image!)
            } else {
                interactor?.saveChannel(name: nameInput.textField.text, url: nil, channelSid: channel?.sid)
            }
        }
    }
    
    @objc func editAction() {
        setEditingState(isEditing: true)
    }
}
