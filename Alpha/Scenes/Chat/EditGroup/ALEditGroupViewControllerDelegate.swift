//
//  ALEditGroupViewControllerDelegate.swift
//  Alpha
//
//  Created by alexandr shitikov on 18.10.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.
//

import UIKit

extension ALEditGroupViewController: UITableViewDelegate, UITableViewDataSource, ALPhotoPickerDelegate {
    
    //MARK: ALPhotoPickerDelegate
    func fileIsSelected(info: [UIImagePickerController.InfoKey : Any]) {
        photoDidChange = true
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            photoImage.image = pickedImage
        }
    }
    
    //MARK: ALEditGroupViewControllerProtocol
    
    func handlePhotoUpload(url: String?) {
        interactor?.saveChannel(name: nameInput.textField.text, url: url, channelSid: channel?.sid)
    }
    
    func handleSaveSuccess() {
        view.stopLoader()
        let data = AlertData(title: String.lc_profileSucceSaveChangesMessage, message: nil, type: .success)
        setEditingState(isEditing: false)
        ALAlertViewController.show(data: data, target: self)
    }
    
    func handleUserList(channels:[ChatChannel]) {
        dataArray.removeAll()
        dataArray = channels
        tableView.reloadData()
    }
    
    //MARK: UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: contactsMainCellId) as! ALContactsMainCell
        let item = dataArray[indexPath.item]
        cell.configure(channel: item)
        return cell
    }
    
    //MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return heightForRow
    }
    
}
