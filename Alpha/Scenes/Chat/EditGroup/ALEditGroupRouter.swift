//
//  ALEditGroupRouter.swift
//  Alpha
//
//  Created by alexandr shitikov on 18.10.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.
//


import UIKit
import TwilioChatClient

@objc protocol ALEditGroupRoutingLogic {
    
}

protocol ALEditGroupDataPassing {
    var dataStore: ALEditGroupInteractorOutputProtocol? { get }
    func navigateToAddUsers(channel:TCHChannel?)
}

class ALEditGroupRouter: NSObject, ALEditGroupRoutingLogic, ALEditGroupDataPassing {
    
    weak var viewController: ALEditGroupViewController?
    var dataStore: ALEditGroupInteractorOutputProtocol?
    let createGroupControllerId = "ALCreateGroupViewController"
    
    // MARK: Routing
    
    func navigateToAddUsers(channel:TCHChannel?) {
        let controller = ALStoryboard.Chat.initController(withId: createGroupControllerId) as! ALCreateGroupViewController
        controller.groupChannel = channel
        controller.screenType = .addContacts
        viewController?.navigationController?.pushViewController(controller, animated: true)
    }
    
    
}
