//
//  ALEditGroupPresenter.swift
//  Alpha
//
//  Created by alexandr shitikov on 18.10.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.
//

import UIKit

protocol ALEditGroupPresentationLogic {
    func handleChannels(channels:[ChatChannel])
    func handlePhotoUpload(url: String?)
    func handleSaveSuccess()
}

class ALEditGroupPresenter: ALEditGroupPresentationLogic {
    
    weak var viewController: ALEditGroupViewControllerProtocol?
    
    // MARK: ALEditGroupPresentationLogic
    
    func handleChannels(channels:[ChatChannel]) {
        viewController?.handleUserList(channels: channels)
    }
    
    func handlePhotoUpload(url: String?) {
        viewController?.handlePhotoUpload(url: url)
    }
    
    func handleSaveSuccess() {
        viewController?.handleSaveSuccess()
    }
    
    
}
