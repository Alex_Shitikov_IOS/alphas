//
//  ALTableHeaderWithTitle.swift
//  Alpha
//
//  Created by alexandr shitikov on 09.10.2020.
//  Copyright © 2020 alexandr shitikov. All rights reserved.
//

import UIKit

class ALTableHeaderWithTitle: UIView {

    @IBOutlet var mainView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("ALTableHeaderWithTitle", owner: self, options: nil)
        addSubview(mainView)
        titleLabel.text = String.lc_favoriteContactsTitle
        mainView.frame = self.bounds
        mainView.clipsToBounds = true
        mainView.backgroundColor = .clear
        titleLabel.textColor = .textGray
    }
    
}
