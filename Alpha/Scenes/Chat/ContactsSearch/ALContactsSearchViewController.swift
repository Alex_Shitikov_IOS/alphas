//
//  ALContactsSearchViewController.swift
//  Alpha
//
//  Created by alexandr shitikov on 08.10.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.


import UIKit

protocol ALContactsSearchDisplayLogic: class {
    func handleChannels(chanels:[ChatChannel])
}

class ALContactsSearchViewController: ALViewController, ALContactsSearchDisplayLogic {
    var interactor: ALContactsSearchBusinessLogic?
    var router: (NSObjectProtocol & ALContactsSearchRoutingLogic & ALContactsSearchDataPassing)?
    
    let searchController = UISearchController(searchResultsController: nil)
    var dataArray = [ChatChannel]()
    var searchArray = [ChatChannel]()
    let contactsMainCellId = "ALContactsMainCell"
    let heightForRow: CGFloat = 90

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var groupButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: Object lifecycle
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: Setup
    
    private func setup() {
        let viewController = self
        let interactor = ALContactsSearchInteractor()
        let presenter = ALContactsSearchPresenter()
        let router = ALContactsSearchRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    // MARK: View lifecycle
    
    override func configureUi() {
        super.configureUi()
        addBackButton()
        title = String.lc_chatNewMessageTitle
        groupButton.setTitleColor(.orangeButton, for: .normal)
        groupButton.layer.borderColor = UIColor.orangeButton.cgColor
        groupButton.layer.borderWidth = 1
        groupButton.rounded(radius: 9)
        groupButton.setTitle(String.lc_chatCreateGroupTitle, for: .normal)
        groupButton.setImage(#imageLiteral(resourceName: "users"), for: .normal)
        groupButton.addTarget(self, action: #selector(navigateToCreateGroup), for: .touchUpInside)
        searchController.searchBar.backgroundColor = .white
        searchController.searchBar.backgroundImage = UIImage()
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = ""
        searchController.searchBar.keyboardAppearance = .light
        searchController.searchBar.searchTextField.textColor = .mainTextBlack
        searchController.searchBar.tintColor = .mainTextBlack
        navigationItem.searchController = searchController
        view.backgroundColor = .white
        navigationController?.navigationBar.backgroundColor = .white
        let headerTableView = ALTableHeaderWithTitle(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 54))
        headerTableView.titleLabel.text = String.lc_chatOrStardDialogTitle
        tableView.tableHeaderView = headerTableView
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: contactsMainCellId, bundle: nil), forCellReuseIdentifier: contactsMainCellId)
        interactor?.getChannels()
        headerView.add(border: .bottom, color: .navigationGray, height: 8, leftOffset: 0)
    }
    
    @objc func navigateToCreateGroup() {
        router?.navigatecToCreateGroup()
    }
        
}
