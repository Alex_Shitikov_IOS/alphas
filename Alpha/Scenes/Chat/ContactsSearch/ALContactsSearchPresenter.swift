//
//  ALContactsSearchPresenter.swift
//  Alpha
//
//  Created by alexandr shitikov on 08.10.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.


import UIKit

protocol ALContactsSearchPresentationLogic {
    func handleChannels(channels:[ChatChannel])
}

class ALContactsSearchPresenter: ALContactsSearchPresentationLogic
{
  weak var viewController: ALContactsSearchDisplayLogic?
  
  // MARK: ALContactsSearchPresentationLogic
    
    func handleChannels(channels:[ChatChannel]) {
        viewController?.handleChannels(chanels: channels)
    }
  
}
