//
//  ALContactsSearchRouter.swift
//  Alpha
//
//  Created by alexandr shitikov on 08.10.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.

import UIKit

@objc protocol ALContactsSearchRoutingLogic {
    func navigatecToCreateGroup()
}

protocol ALContactsSearchDataPassing
{
    var dataStore: ALContactsSearchDataStore? { get }
}

class ALContactsSearchRouter: NSObject, ALContactsSearchRoutingLogic, ALContactsSearchDataPassing
{
    weak var viewController: ALContactsSearchViewController?
    var dataStore: ALContactsSearchDataStore?
    let createGroupControllerId = "ALCreateGroupViewController"
    
    // MARK: Routing
    
    func navigatecToCreateGroup() {
        let controller = ALStoryboard.Chat.initController(withId: createGroupControllerId)
        viewController?.navigationController?.pushViewController(controller, animated: true)
    }
    
}
