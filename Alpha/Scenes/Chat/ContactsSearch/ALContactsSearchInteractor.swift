//
//  ALContactsSearchInteractor.swift
//  Alpha
//
//  Created by alexandr shitikov on 08.10.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.
//
//

import UIKit
import TwilioChatClient

protocol ALContactsSearchBusinessLogic {
    func getChannels()
}

protocol ALContactsSearchDataStore {
    //var name: String { get set }
}

class ALContactsSearchInteractor: ALContactsSearchBusinessLogic, ALContactsSearchDataStore {
    var presenter: ALContactsSearchPresentationLogic?
    var worker: ALContactsSearchWorker?
    let workGroup = DispatchGroup()
    
    // MARK: ALContactsSearchBusinessLogic
    
    func getChannels() {
        var users = [ChatChannel]()
            ChannelManager.sharedManager.generalChannel.members?.membersList().forEach({ (member) in
                workGroup.enter()
                member.userDescriptor { (result, userDescription) in
                    let attributes = UserInfoAttributes(attributes: userDescription?.attributes()?.dictionary)
                    let user = ChatChannel(channelDescription: nil,
                                           userDescription: userDescription,
                                           title: userDescription?.friendlyName ?? "unknown",
                                           isFavorite: false,
                                           photoUrl:attributes.photoUrl,
                                           uniqueName: userDescription?.identity ?? "",
                                           channelType: .user,
                                           channel: nil)
                    if userDescription?.identity != MessagingManager.sharedManager().client?.user?.identity {
                        users.append(user)
                    }
                    self.workGroup.leave()
                }
            })
        
        workGroup.notify(queue: .main) {
            self.presenter?.handleChannels(channels: users)
        }
    }
    
}
