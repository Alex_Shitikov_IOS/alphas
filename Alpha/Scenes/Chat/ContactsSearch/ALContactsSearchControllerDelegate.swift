//
//  ALContactsSearchControllerDelegate.swift
//  Alpha
//
//  Created by alexandr shitikov on 08.10.2020.
//  Copyright (c) 2020 alexandr shitikov. All rights reserved.

import UIKit

extension ALContactsSearchViewController: UISearchResultsUpdating, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: UISearchResultsUpdating
    
    func updateSearchResults(for searchController: UISearchController) {
        updateTableWithSearchParams()
    }
    
    //MARK: UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:contactsMainCellId) as! ALContactsMainCell
        let item = searchArray[indexPath.item]
        cell.configure(channel: item)
        return cell
    }
    
    //MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return heightForRow
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = searchArray[indexPath.item]
        view.startLoader()
        ChannelManager.sharedManager.chatWithUser(userIdentity: item.userDescription?.identity ?? "") { (channel) in
            self.view.stopLoader()
            if let userChannel = channel {
                DispatchQueue.main.async {
                    ALChatContactsRouter().navigateToChat(channel: userChannel, chatChannel: item)
                }
            } else {
                let data = AlertData(title: String.lc_serverErrorMessage, message: "", type: .error)
                ALAlertViewController.show(data: data, target: self)
            }
        }
    }
    
    //MARK: ALContactsSearchDisplayLogic
    
    func handleChannels(chanels:[ChatChannel]) {
        tableView.hidePaceholder()
        dataArray.removeAll()
        dataArray = chanels
        updateTableWithSearchParams()
    }
    
    func updateTableWithSearchParams() {
        if searchController.searchBar.text?.isEmpty ?? true {
            searchArray.removeAll()
            searchArray = dataArray
        } else {
            searchArray.removeAll()
            searchArray = dataArray.all(where: {$0.title.lowercased().contains(searchController.searchBar.text!.lowercased())})
        }
        if searchArray.isEmpty {
            tableView.showPaceholder(type: .noContacts)
        } else {
            tableView.hidePaceholder()
        }
        tableView.reloadData()
    }
}
