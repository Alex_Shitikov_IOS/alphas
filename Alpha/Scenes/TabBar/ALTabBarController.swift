//
//  ALTabBarController.swift
//  Alpha
//
//  Created by alexandr shitikov on 19.07.2020.
//  Copyright © 2020 alexandr shitikov. All rights reserved.
//

import UIKit

class ALTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        UITabBar.appearance().tintColor = .orangeButton

    }
    
}
