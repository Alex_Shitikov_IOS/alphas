//
//  ViewController.swift
//  DRMTest
//
//  Created by alexandr shitikov on 28.01.2021.
//
import Foundation
import AVFoundation
import UIKit

class ViewController: UIViewController {
    
    var player = DRMFairplay()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        guard let url = URL(string: "https://svs.itworkscdn.net/itwlive/lbclive2.smil/playlist.m3u8") else {return}
        let asset = AVURLAsset(url:url)
        player.play(asset: asset)
    }


}

